//START CORS Loading
function postCORS(url, data, callback, type)
{
	if (jQuery.browser.msie && window.XDomainRequest) {
        // Use XDR
		var params = '&clientCode='+data.clientCode+'&pageUrl='+data.pageUrl+'&pageHost='+data.pageHost;
        var xdr = new XDomainRequest();
        xdr.open("post", url);
        setTimeout(function(){
			xdr.send(params);
		}, 0);
        xdr.onload = function() {
            callback(handleXDROnload(this, type));
        };
		xdr.onprogress = function(){};
		xdr.ontimeout = function(){};
		xdr.onerror = function(){};
    } else if (!jQuery.browser.msie) {
		jQuery.post(url, data, callback, type);
	} else {
		var output = '<table width="600" border="0" cellspacing="0" cellpadding="0"><tr><td align="center" style="font-family:Arial, Helvetica, sans-serif; padding:10px;"><h1>Browser Not Supported</h1></td></tr><tr><td align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:16px; padding:10px;">This content requires <a href="http://windows.microsoft.com/en-us/internet-explorer/products/ie/home/">Internet Explorer 8+</a>, <a href="http://www.mozilla.org/en-US/firefox/new/">Firefox 3.5+</a>, <a href="http://www.apple.com/safari/download/">Safari 4+</a>, or <a href="https://www.google.com/intl/en/chrome/browser/">Google Chrome</a>.<br /><br />Please update your browser to view this content.</td></tr></table>';
		callback(output);
	}
}
function handleXDROnload(xdr, type)
{
    var responseText = xdr.responseText, dataType = type || "";
    if (dataType.toLowerCase() == "xml"
        && typeof responseText == "string") {
        // If expected data type is xml, we need to convert it from a
        // string to an XML DOM object
        var doc;
        try {
            if (window.ActiveXObject) {
                doc = new ActiveXObject('Microsoft.XMLDOM');
                doc.async = 'false';
				
                doc.loadXML(responseText);
            } else {
                var parser = new DOMParser();
                doc = parser.parseFromString(responseText, 'text/xml');
            }
            return doc;
        } catch(e) {
            // ERROR parsing XML for conversion, just return the responseText
        }
    }
    return responseText;
}
//END CORS Loading


//START JSON Loading
(function(a){function b(){}function c(a){A=[a]}function d(a,b,c,d){try{d=a&&a.apply(b.context||b,c)}catch(e){d=!1}return d}function e(a){return/\?/.test(a)?"&":"?"}function D(l){function Y(a){Q++||(R(),K&&(y[M]={s:[a]}),G&&(a=G.apply(l,[a])),d(D,l,[a,t]),d(F,l,[l,t]))}function Z(a){Q++||(R(),K&&a!=u&&(y[M]=a),d(E,l,[l,a]),d(F,l,[l,a]))}l=a.extend({},B,l);var D=l.success,E=l.error,F=l.complete,G=l.dataFilter,H=l.callbackParameter,I=l.callback,J=l.cache,K=l.pageCache,L=l.charset,M=l.url,N=l.data,O=l.timeout,P,Q=0,R=b,S,T,U,V,W,X;return w&&w(function(a){a.done(D).fail(E),D=a.resolve,E=a.reject}).promise(l),l.abort=function(){!(Q++)&&R()},d(l.beforeSend,l,[l])===!1||Q?l:(M=M||h,N=N?typeof N=="string"?N:a.param(N,l.traditional):h,M+=N?e(M)+N:h,H&&(M+=e(M)+encodeURIComponent(H)+"=?"),!J&&!K&&(M+=e(M)+"_"+(new Date).getTime()+"="),M=M.replace(/=\?(&|$)/,"="+I+"$1"),K&&(P=y[M])?P.s?Y(P.s[0]):Z(P):(v[I]=c,V=a(s)[0],V.id=k+z++,L&&(V[g]=L),C&&C.version()<11.6?(W=a(s)[0]).text="document.getElementById('"+V.id+"')."+n+"()":V[f]=f,p in V&&(V.htmlFor=V.id,V.event=m),V[o]=V[n]=V[p]=function(a){if(!V[q]||!/i/.test(V[q])){try{V[m]&&V[m]()}catch(b){}a=A,A=0,a?Y(a[0]):Z(i)}},V.src=M,R=function(a){X&&clearTimeout(X),V[p]=V[o]=V[n]=null,x[r](V),W&&x[r](W)},x[j](V,U=x.firstChild),W&&x[j](W,U),X=O>0&&setTimeout(function(){Z(u)},O)),l)}var f="async",g="charset",h="",i="error",j="insertBefore",k="_jqjsp",l="on",m=l+"click",n=l+i,o=l+"load",p=l+"readystatechange",q="readyState",r="removeChild",s="<script>",t="success",u="timeout",v=window,w=a.Deferred,x=a("head")[0]||document.documentElement,y={},z=0,A,B={callback:k,url:location.href},C=v.opera;D.setup=function(b){a.extend(B,b)},a.jsonp=D})(jQuery)
//END JSON Loading


$(document).ready(function(){
	var jPathname = window.location.pathname;
        var jPathurl = window.location.protocol + "//" + window.location.host;
		var cCode = "DDCT";
        var DOMurl = "http://cms.dealeronlinemarketing.com/Processor.php";
        var DOMscripturl = "http://cms.dealeronlinemarketing.com/Tracker.php";
	postCORS(DOMurl, {clientCode : cCode, pageUrl : jPathname, pageHost : jPathurl }, function(data){
		if (data.length > 0){
                      $("#dom_dynamic").html(data);                        
		}
	});
        /*postCORS(DOMscripturl, {clientCode : cCode, pageUrl : jPathname, pageHost : jPathurl }, function(data1){
		if (data1.length > 0){
                    // alert(data1);
					//document.getElementsByTagName('body').appendChild(data1);
                    document.getElementById("dom_script1").innerHTML = data1;
                }
	});*/
	//http://www.willmaster.com/library/optimization/search_engine_spider-friendly_javascript_content.php
	//src="http://testing.dealeronlinemarketing.com/clickheat/js/DOM360script.js"
});

