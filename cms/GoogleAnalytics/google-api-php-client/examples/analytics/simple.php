<?php
require_once '../../src/Google_Client.php';
require_once '../../src/contrib/Google_AnalyticsService.php';
session_start();

$client = new Google_Client();
$client->setApplicationName("Google Analytics PHP Starter Application");

// Visit https://code.google.com/apis/console?api=analytics to generate your
// client id, client secret, and to register your redirect uri.
$client->setClientId('170027429160.apps.googleusercontent.com');
$client->setClientSecret('kMAavg_InUyakJwcHolLQsFn');
$client->setRedirectUri('http://rob.com/google_analytics/google-api-php-client/examples/analytics/simple.php');
$client->setDeveloperKey('AIzaSyCPrBMaamOoxDEuwb0Y65mIKdjDQuZX9CY');
$service = new Google_AnalyticsService($client);

if (isset($_GET['logout'])) {
  unset($_SESSION['token']);
}

if (isset($_GET['code'])) {
  $client->authenticate();
  $_SESSION['token'] = $client->getAccessToken();
  $redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
  header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
}

if (isset($_SESSION['token'])) {
  $client->setAccessToken($_SESSION['token']);
}

if ($client->getAccessToken()) {
  //$props = $service->management_webproperties->listManagementWebproperties("~all");
  //echo "Web property ID: ".$props['items'][1]['id']."<br>";
  //echo "Account ID: ".$props['items'][1]['accountId']."<br>";
  //print "<h1>Web Properties</h1><pre>" . print_r($props, true) . "</pre>";
  //$account = $props['items'][1]['accountId'];
  //$web_prop = $props['items'][1]['id'];
  //$cds = $service->management_customDataSources->listManagementCustomDataSources($account,$web_prop);
  //print_r($cds);

  $accounts = $service->management_accounts->listManagementAccounts();
  $account_id = $accounts['items'][1]['id'];
  echo "Account ID: ".$accounts['items'][1]['id']."<br>";
  echo "Account Name: ".$accounts['items'][1]['name']."<br>";
  //print "<h1>Accounts</h1><pre>" . print_r($accounts, true) . "</pre>";
  
  $web_props = $service->management_webproperties->listManagementWebproperties($account_id);
  $web_prop_id = $web_props['items'][1]['id'];
  echo "Web Prop Id: ".$web_prop_id."<br>";
  
  $profiles = $service->management_profiles->listManagementProfiles($account_id,$web_prop_id);
  print_r($profiles);
  $profile_id = $profiles['items'][1]['profileId'];
  echo "Profile Id: ".$profile_id."<br>";
  
  //$dat_account = $accounts['items'][1]['id'];
  //$daily_uploads = $service->management_dailyUploads->listManagementDailyUploads

  //$segments = $service->management_segments->listManagementSegments();
  //print "<h1>Segments</h1><pre>" . print_r($segments, true) . "</pre>";

  //$goals = $service->management_goals->listManagementGoals("~all", "~all", "~all");
  //print "<h1>Segments</h1><pre>" . print_r($goals, true) . "</pre>";
  
  //$profiles = $service->management_goals->listManagementGoals("~all", "~all", "~all");
  //$profiles = $service->management_goals->listManagementGoals("~all", "~all", "~all");
  echo "Account Id: ".$profiles['items'][1]['accountId']."<br>";
  echo "Profile Id: ".$profiles['items'][1]['profileId']."<br>";
  echo "Profile Active: ".$profiles['items'][1]['active']."<br>";
  echo "Page Id: ".$profiles['items'][1]['id']."<br>";
  print "<h1>Profiles</h1><pre>" . print_r($profiles, true) . "</pre>";
  
  $dat_profile = 'ga:'.$profiles['items'][1]['profileId'];
  $start_date = '2008-01-01';
  $end_date = '2013-07-23';
  $metrics = 'ga:visits,ga:avgTimeOnPage,ga:pageviews,ga:visitBounceRate';
  $options = array(
            'dimensions' => 'ga:source,ga:keyword',
            'sort' => '-ga:visits,ga:keyword',
            'filters' => 'ga:medium==organic',
            'max-results' => '25');
  $dat_profile_visits = $service->data_ga->get($dat_profile,$start_date,$end_date,$metrics);
  echo "Profile Name: ".$dat_profile_visits['profileInfo']['profileName']."<br>";
  echo "Total Visits: ".$dat_profile_visits['totalsForAllResults']['ga:visits']."<br>";
  echo "Total PageViews: ".$dat_profile_visits['totalsForAllResults']['ga:pageviews']."<br>";
  echo "Bounce Rate: ".$dat_profile_visits['totalsForAllResults']['ga:visitBounceRate']."<br>";
  echo "Avg Time on Page: ".$dat_profile_visits['totalsForAllResults']['ga:avgTimeOnPage']."<br>";
  
  print_r($dat_profile_visits);

  $_SESSION['token'] = $client->getAccessToken();
} else {
  $authUrl = $client->createAuthUrl();
  print "<a class='login' href='$authUrl'>Connect Me!</a>";
}