<?php
$index = 0;
while($index < count($the_profiles)){
  $dat_profile_id = $the_profiles[$index]['profile'];
  //echo "Started processing ".$dat_profile_id."\r\n";
  $dat_web_id = $the_profiles[$index]['web_id'];
  $dat_web_url = $the_profiles[$index]['web_url'];
  
  //check it to make sure this is a valid profile id for Google, otherwise google throws Error 500
  if(in_array($dat_profile_id,$all_profile_ids)){
	$num_days_index = 0;
	while($num_days_index < $num_days){
		//set_time_limit(300);
		$dat_profile = 'ga:'.$dat_profile_id;
		$run_date = new DateTime($start_date);
		$run_date->add(new DateInterval('P'.$num_days_index.'D'));
		$run_date = date_format($run_date,'Y-m-d');
		$metrics = 'ga:visits,ga:timeOnPage,ga:timeOnSite,ga:pageviews,ga:uniquePageviews,ga:bounces,ga:newVisits,ga:visitors';
		
		/*get google analytics for all pages*/
		//usleep(100000); //google will get mad if 10 requests are made more than every secend
		try{
			$dat_profile_visits = $service->data_ga->get($dat_profile,$run_date,$run_date,$metrics);
			$total_visits = $dat_profile_visits['totalsForAllResults']['ga:visits'];
			$total_visits_new = $dat_profile_visits['totalsForAllResults']['ga:newVisits'];
			$total_page_views = $dat_profile_visits['totalsForAllResults']['ga:pageviews'];
			$total_unique_page_views = $dat_profile_visits['totalsForAllResults']['ga:uniquePageviews'];
			$total_bounces = round($dat_profile_visits['totalsForAllResults']['ga:bounces']);
			$total_time_on_page = round($dat_profile_visits['totalsForAllResults']['ga:timeOnPage']);
			$total_time_on_site = round($dat_profile_visits['totalsForAllResults']['ga:timeOnSite']);
			$total_visitors = $dat_profile_visits['totalsForAllResults']['ga:visitors'];
			
			//insert into GoogleAnalyticsByDate with filter = none
			$filter = 'none';
			$dat_time_stamp = date('Y-m-d H:i:s');
			//check to see if the record exists already: if so, update, else insert (based on unique combo of date,profile_id,filter)
			$sql = "UPDATE `GoogleAnalyticsByDate` 
			        SET `total_visits`='".$total_visits."',`time_on_page`='".$total_time_on_page."',`time_on_site`='".$total_time_on_site."',`page_views`='".$total_page_views."',`unique_page_views`='".$total_unique_page_views."',`bounces`='".$total_bounces."',`new_visits`='".$total_visits_new."',`visitors`='".$total_visitors."',`updated`='".$dat_time_stamp."'
			        WHERE `date`='".$run_date."' AND `profile_id`='".$dat_profile_id."' AND `filter`='".$filter."' LIMIT 1;";
			mysql_query($sql) or die(mysql_error($dbh));
			if (mysql_affected_rows()==0){ //this means the record did not exist, so insert a new one
				$sql = "INSERT INTO `GoogleAnalyticsByDate`
			    	    (`profile_id`,`date`,`total_visits`,`time_on_page`,`time_on_site`,`page_views`,`unique_page_views`,`bounces`,`new_visits`,`visitors`,`filter`,`updated`)
			        	VALUES ('".$dat_profile_id."','".$run_date."','".$total_visits."','".$total_time_on_page."','".$total_time_on_site."','".$total_page_views."','".$total_unique_page_views."','".$total_bounces."','".$total_visits_new."','".$total_visitors."','".$filter."','".$dat_time_stamp."');";
				
				mysql_query($sql) or die(mysql_error($dbh));
			}
			
			/*get google analytics for service pages*/
			//retrieve service pages from our table based on the WEB_ID
			$sql = "SELECT `ServiceCenterPages`.`SERVICE_Url`
			        FROM `ServiceCenterPages`
			        WHERE `ServiceCenterPages`.`WEB_ID` = '".$dat_web_id."';";
			$service_pages = mysql_query($sql) or die(mysql_error($dbh));
			$num_service_pages = mysql_num_rows($service_pages);
			$filters = '';
			for($i=0;$i<$num_service_pages;$i++){
				$service_page = mysql_fetch_assoc($service_pages);
				$filters .= "ga:pagePath==".$service_page['SERVICE_Url'].",";
			}
			if(strlen($filters) == 0){
				//echo "No service pages for profile id ".$dat_profile_id;
			}
			else{
				$filters = str_replace($dat_web_url,'',$filters);  //remove the last comma from the filters
				$filters = substr($filters,0,-1);
				//echo $filters;
				$options = array(
			    	      /*'dimensions' => '',*/
			        	  /*'sort' => '',*/
			          	  'filters' => $filters);
			          	  /*'max-results' => '')*/
				//usleep(100000); //google will get mad if 10 requests are made more than every secend
			    $dat_profile_visits = $service->data_ga->get($dat_profile,$run_date,$run_date,$metrics,$options);
				$service_visits = $dat_profile_visits['totalsForAllResults']['ga:visits'];
				$service_visits_new = $dat_profile_visits['totalsForAllResults']['ga:newVisits'];
				$service_page_views = $dat_profile_visits['totalsForAllResults']['ga:pageviews'];
				$service_unique_page_views = $dat_profile_visits['totalsForAllResults']['ga:uniquePageviews'];
				$service_bounces = round($dat_profile_visits['totalsForAllResults']['ga:bounces']);
				$service_time_on_page = round($dat_profile_visits['totalsForAllResults']['ga:timeOnPage']);
				$service_time_on_site = round($dat_profile_visits['totalsForAllResults']['ga:timeOnSite']);
				$service_visitors = $dat_profile_visits['totalsForAllResults']['ga:visitors'];
				
				//insert into GoogleAnalyticsByDate with filter = service
				$filter = 'service';
				$dat_time_stamp = date('Y-m-d H:i:s');
				//check to see if the record exists already: if so, update, else insert (based on unique combo of date,profile_id,filter)
				$sql = "UPDATE `GoogleAnalyticsByDate` 
				        SET `total_visits`='".$service_visits."',`time_on_page`='".$service_time_on_page."',`time_on_site`='".$service_time_on_site."',`page_views`='".$service_page_views."',`unique_page_views`='".$service_unique_page_views."',`bounces`='".$service_bounces."',`new_visits`='".$service_visits_new."',`visitors`='".$service_visitors."',`updated`='".$dat_time_stamp."'
				        WHERE `date`='".$run_date."' AND `profile_id`='".$dat_profile_id."' AND `filter`='".$filter."' LIMIT 1;";
				mysql_query($sql) or die(mysql_error($dbh));
				if (mysql_affected_rows()==0){ //this means the record did not exist, so insert a new one
					//echo "No record to update (".mysql_affected_rows()."), so then insert"."\r\n";
					$sql = "INSERT INTO `GoogleAnalyticsByDate`
				    	    (`profile_id`,`date`,`total_visits`,`time_on_page`,`time_on_site`,`page_views`,`unique_page_views`,`bounces`,`new_visits`,`visitors`,`filter`,`updated`)
				        	VALUES ('".$dat_profile_id."','".$run_date."','".$service_visits."','".$service_time_on_page."','".$service_time_on_site."','".$service_page_views."','".$service_unique_page_views."','".$service_bounces."','".$service_visits_new."','".$service_visitors."','".$filter."','".$dat_time_stamp."');";
					
					mysql_query($sql) or die(mysql_error($dbh));
				}
			}
			//echo $run_date." complete."."\r\n";
			$num_days_index++;
		} catch (Google_ServiceException $e){
			echo $e->getMessage()."\r\n";
			sleep(1);
		} catch (Google_IOException $e){
			echo $e->getMessage()."\r\n";
			sleep(1);
		}
	}
  }
  else{
	//profile in database did not match a profile that is connected to the webmaster analytics account
	echo "Invalid profile id ".$dat_profile_id." for website ".$dat_web_url."\r\n";	
  }
  
  //echo "completed profile ".$dat_profile_id."\r\n";
  $index++;
}

echo "Completed ".basename(__FILE__)."\r\n";
?>