<?php
$index = 0;
while($index < count($the_profiles)){
$dat_profile_id = $the_profiles[$index]['profile'];
//echo "Started processing ".$dat_profile_id."\r\n";
$dat_web_id = $the_profiles[$index]['web_id'];
$dat_web_url = $the_profiles[$index]['web_url'];

//check it to make sure this is a valid profile id for Google, otherwise google will throw Error 500
if(in_array($dat_profile_id,$all_profile_ids)){
	$num_days_index = 0;
	while($num_days_index < $num_days){
		$dat_profile = 'ga:'.$dat_profile_id;
		$run_date = new DateTime($start_date);
		$run_date->add(new DateInterval('P'.$num_days_index.'D'));
		$run_date = date_format($run_date,'Y-m-d');
		$metrics = 'ga:visitors';
		$options = array(
		    	      'dimensions' => 'ga:browser');
		        	  /*'sort' => '',*/
		          	  /*'filters' => $filters),*/
		          	  /*'max-results' => '')*/
		
		/*get google analytics total website visitor data by the ga:browser dimension*/
		//usleep(100000); //google will get mad if 10 requests are made more than every secend
		try{
			$dat_profile_visits = $service->data_ga->get($dat_profile,$run_date,$run_date,$metrics,$options);
			$num_browsers = count($dat_profile_visits['rows']);
			
			//loop through each source and plug info into GoogleAnalyticsTrafficSources
			$num_browsers_index = 0;
			while($num_browsers_index < $num_browsers){
				$browser = mysql_real_escape_string($dat_profile_visits['rows'][$num_browsers_index][0]);
				$visitors =  mysql_real_escape_string($dat_profile_visits['rows'][$num_browsers_index][1]);
				
				//insert into GoogleAnalyticsBrowser with filter = none
				$filter = 'none';
				$dat_time_stamp = date('Y-m-d H:i:s');
				//check to see if the record exists already: if so, update, else insert (based on unique combo of date,profile_id,filter)
				$sql = "UPDATE `GoogleAnalyticsBrowser` 
				        SET `visitors`='".$visitors."',`updated`='".$dat_time_stamp."'
				        WHERE `date`='".$run_date."' AND `profile_id`='".$dat_profile_id."' AND `filter`='".$filter."' AND `browser`='".$browser."' LIMIT 1;";
				mysql_query($sql) or die(mysql_error($dbh));
				if (mysql_affected_rows()==0){ //this means the record did not exist, so insert a new one
					$sql = "INSERT INTO `GoogleAnalyticsBrowser`
				    	    (`profile_id`,`date`,`browser`,`visitors`,`filter`,`updated`)
				        	VALUES ('".$dat_profile_id."','".$run_date."','".$browser."','".$visitors."','".$filter."','".$dat_time_stamp."');";
					
					mysql_query($sql) or die(mysql_error($dbh));
				}
				$num_browsers_index++;
			}
			/*get google analytics visitor data for service pages by the ga:browser dimension*/
			//retrieve service pages from our table based on the WEB_ID
			$sql = "SELECT `ServiceCenterPages`.`SERVICE_Url`
			        FROM `ServiceCenterPages`
			        WHERE `ServiceCenterPages`.`WEB_ID` = '".$dat_web_id."';";
			$service_pages = mysql_query($sql) or die(mysql_error($dbh));
			$num_service_pages = mysql_num_rows($service_pages);
			$filters = '';
			//loop through service pages from table and add each of them to a filters string so google knows only to use these pages
			for($i=0;$i<$num_service_pages;$i++){
				$service_page = mysql_fetch_assoc($service_pages);
				$filters .= "ga:pagePath==".$service_page['SERVICE_Url'].",";
			}
			if(strlen($filters) == 0){
				//echo "No service pages for profile id ".$dat_profile_id;
			}
			else{
				$filters = str_replace($dat_web_url,'',$filters);  //remove the last comma from the filters
				$filters = substr($filters,0,-1);
				$options = array(
			    	      'dimensions' => 'ga:browser',
			          	  'filters' => $filters);
			    //usleep(100000); //google will get mad if 10 requests are made more than every secend
			    $dat_profile_visits = $service->data_ga->get($dat_profile,$run_date,$run_date,$metrics,$options);
			    $num_browsers = count($dat_profile_visits['rows']);
			    
			    //loop through each source and plug info into GoogleAnalyticsTrafficSources
			    $num_browsers_index = 0;
				while($num_browsers_index < $num_browsers){
					$browser = mysql_real_escape_string($dat_profile_visits['rows'][$num_browsers_index][0]);
					$visitors =  mysql_real_escape_string($dat_profile_visits['rows'][$num_browsers_index][1]);
					
					//insert into GoogleAnalyticsBrowser with filter = service
					$filter = 'service';
					$dat_time_stamp = date('Y-m-d H:i:s');
					//check to see if the record exists already: if so, update, else insert (based on unique combo of date,profile_id,filter)
					$sql = "UPDATE `GoogleAnalyticsBrowser` 
					        SET `visitors`='".$visitors."',`updated`='".$dat_time_stamp."'
					        WHERE `date`='".$run_date."' AND `profile_id`='".$dat_profile_id."' AND `filter`='".$filter."' AND `browser`='".$browser."' LIMIT 1;";
					mysql_query($sql) or die(mysql_error($dbh));
					if (mysql_affected_rows()==0){ //this means the record did not exist, so insert a new one
						//echo "No record to update (".mysql_affected_rows()."), so then insert"."\r\n";
						$sql = "INSERT INTO `GoogleAnalyticsBrowser`
					    	    (`profile_id`,`date`,`browser`,`visitors`,`filter`,`updated`)
					        	VALUES ('".$dat_profile_id."','".$run_date."','".$browser."','".$visitors."','".$filter."','".$dat_time_stamp."');";
						
						mysql_query($sql) or die(mysql_error($dbh));
					}
					$num_browsers_index++;
				}
			}
			//echo $run_date." complete."."\r\n";
			$num_days_index++;
		} catch (Google_ServiceException $e){
			echo $e->getMessage()."\r\n";
			sleep(1);
		} catch (Google_IOException $e){
			echo $e->getMessage()."\r\n";
			sleep(1);
		}
	}
  }
  else{
	//profile in database did not match a profile that is connected to the webmaster analytics account
	echo "Invalid profile id ".$dat_profile_id." for website ".$dat_web_url."\r\n";	
  }
//echo "Completed profile ".$dat_profile_id."\r\n";
$index++;
}

echo "Completed ".basename(__FILE__)."\r\n";
?>