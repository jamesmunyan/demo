<?php
//get profile ids from Google Analytics API	
$profiles = $service->management_profiles->listManagementProfiles("~all", "~all");
$total_profs = count($profiles['items']);
$num_new = 0;
$all_profile_ids = array();
for($i=0;$i<$total_profs;$i++){
  	$id = $profiles['items'][$i]['id'];
  	$name = $profiles['items'][$i]['name'];
  	$ua = $profiles['items'][$i]['webPropertyId'];
  	$url = $profiles['items'][$i]['websiteUrl'];
  	$created = substr($profiles['items'][$i]['created'],0,10);
  	
  	//check to make sure the profile is an analytics profile
  	if($profiles['items'][$i]['kind'] == "analytics#profile"){
    	
    	//insert the profile data into our database
    	$dat_time_stamp = date('Y-m-d H:i:s');
		//check to see if the record exists already: if so, update, else insert (based on unique combo of date,profile_id,filter)
		$sql = "UPDATE `GoogleAnalyticsProfiles` 
		        SET `ua_code`='".$ua."',`profile_name`='".$name."',`website_url`='".$url."',`google_created_date`='".$created."',`updated`='".$dat_time_stamp."'
		        WHERE `profile_id`='".$id."' LIMIT 1;";
		mysql_query($sql) or die(mysql_error($dbh));
		//if the record did not already exists, insert a new one
		if (mysql_affected_rows()==0){
			$num_new = $num_new + 1;
			$sql = "INSERT INTO `GoogleAnalyticsProfiles`
		    	    (`ua_code`,`profile_id`,`profile_name`,`website_url`,`google_created_date`,`updated`)
		        	VALUES ('".$ua."','".$id."','".$name."','".$url."','".$created."','".$dat_time_stamp."');";
			
			mysql_query($sql) or die(mysql_error($dbh));
		}
  	}
} //end forloop iteration of $profiles
  
//echo out results to screen
$num_updated = $total_profs - $num_new;
echo "# profiles updated: ".$num_updated."\r\n";
echo "# new profiles added: ".$num_new."\r\n";
echo "Completed".basename(__FILE__)."\r\n";
?>