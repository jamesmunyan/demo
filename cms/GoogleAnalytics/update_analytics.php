<?php
require_once 'google-api-php-client/src/Google_Client.php';
require_once 'google-api-php-client/src/contrib/Google_AnalyticsService.php';
require_once 'db_connect.php';

//configurables
$access_token = '{"access_token":"ya29.AHES6ZSReTKUddWKeLQDrcQFULpZhAmE4tairjDJ_hxAR2VX193L","token_type":"Bearer","expires_in":3600,"refresh_token":"1\/2pe2vYh-hjt23VrGQ23nNdsT0-iV_sCPcsLWiBcvRUA","created":1377204022}';
$start_date = date('Y-m-d',time()-60*60*24); //yesterday's date formatted YYY-MM-DD
$num_days = 1;

$client = new Google_Client();
$client->setApplicationName('DOM360 GA Update');
$client->setClientId('170027429160.apps.googleusercontent.com');
$client->setClientSecret('kMAavg_InUyakJwcHolLQsFn');
$client->setDeveloperKey('AIzaSyCPrBMaamOoxDEuwb0Y65mIKdjDQuZX9CY');
$service = new Google_AnalyticsService($client);

$client->setAccessToken($access_token);

if ($client->getAccessToken()) {
  //authenticated successfully with Google Analytics API
  echo "Successfully authenticated to Google Analytics API."."\r\n";

  //get profile ids from Google Analytics API	
  $profiles = $service->management_profiles->listManagementProfiles("~all", "~all");
  $total_profs = count($profiles['items']);
  $all_profile_ids = array();
  for($i=0;$i<$total_profs;$i++){
	$all_profile_ids[] = $profiles['items'][$i]['id'];
  }

  //get all active profile ids from our table Websites
  $sql = "SELECT `Websites`.`WEB_ID`,`Websites`.`WEB_GoogleProfileID`,`Websites`.`WEB_Url`
        FROM `Websites`
        WHERE `Websites`.`WEB_Active`=1 AND `Websites`.`WEB_GoogleProfileID`!=0;";
  $profile_ids = mysql_query($sql) or die(mysql_error($dbh));
  $the_profiles = array();
  $num_profile_ids = mysql_num_rows($profile_ids);
  $profile_index = 0;
  while($profile_record = mysql_fetch_assoc($profile_ids)){
    $the_profiles[$profile_index]['profile'] = $profile_record['WEB_GoogleProfileID'];
    $the_profiles[$profile_index]['web_id'] = $profile_record['WEB_ID'];
    $the_profiles[$profile_index]['web_url'] = $profile_record['WEB_Url'];

    $profile_index++;
  }
  echo "Num profiles: ".count($the_profiles)."\r\n";

  //require various metric scripts to pull data into database
  require_once 'scripts/updateAnalyticsByDate.php';
  require_once 'scripts/updateBrowser.php';
  require_once 'scripts/updateMobile.php';
  require_once 'scripts/updateMobileYesNo.php';
  require_once 'scripts/updateOperSys.php';
  require_once 'scripts/updateSearchEngine.php';
  require_once 'scripts/updateTrafficSourcesByDate.php';
  require_once 'scripts/updateTrafficSourcesKeyword.php';
  require_once 'scripts/updatePages.php';
  require_once 'scripts/updateProfiles.php';

}
else {
  echo "Error connecting to Google API."."\r\n";
}
?>