<?php 
require_once("./Connections/ContentConnection.php");
// Specify domains from which requests are allowed
header('Access-Control-Allow-Origin: *');

// Specify which request methods are allowed
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');

/*
 * jQuery < 1.4.0 adds an X-Requested-With header which requires pre-flighting
 * requests. This involves an OPTIONS request before the actual GET/POST to 
 * make sure the client is allowed to send the additional headers.
 * We declare what additional headers the client can send here.
 */

// Additional headers which may be sent along with the CORS request
header('Access-Control-Allow-Headers: X-Requested-With');

// Set the age to 1 day to improve speed/caching.
header('Access-Control-Max-Age: 86400');

// Exit early so the page isn't fully loaded for options requests
if (strtolower($_SERVER['REQUEST_METHOD']) == 'options') {
    exit();
}
if (isset($HTTP_RAW_POST_DATA)) {
    $data = explode('&', $HTTP_RAW_POST_DATA);
    foreach ($data as $val) {
        if (!empty($val)) {
            list($key, $value) = explode('=', $val);   
            $_POST[$key] = urldecode($value);
        }
    }
}
// Create Connection to database
mysql_select_db($database_SrvSite, $SrvSite);

// Set Post Vars as local vars
$pageHost = $_POST['pageHost'];
$pageUrl = $_POST['pageUrl'];
$clientCode = $_POST['clientCode'];
$string = '';
// Build Query for checking referrer url to local database limit by 1 so no dups.
// No website should be in the databse twice

//Execute Query
$WEBID_Query= "SELECT * FROM Websites Where WEB_Url='".$pageHost."' LIMIT 1";
$WEB_IDs = mysql_query($WEBID_Query);
//echo $pageHost.'-'.$pageUrl;

// Build array of data
$WEBID_Array = mysql_fetch_array($WEB_IDs);

// Test output of query
// echo "Info: ".$WEBID_Array['WEB_Active'].' '.$WEBID_Array['WEB_ID'];
$check =1;
// Check for Active Web Else 404 error.
//if($WEBID_Array['WEB_Active']==1){
if($check == 1 ){
	$CONTENT_Query = "SELECT * FROM Contents Where WEB_ID='".$WEBID_Array['WEB_ID']. "' and CONTENT_UrlPath = '".$pageUrl."' and CONTENT_Active = '1' ORDER BY CONTENT_ID DESC LIMIT 1";
	
	$CONTENT_Code = mysql_query($CONTENT_Query);
	if (mysql_num_rows($CONTENT_Code)>0){
		while($CODE = mysql_fetch_object($CONTENT_Code)){
			$string .= file_get_contents($CODE->CONTENT_UrlSource);
			$CONTENTID = $CODE->CONTENT_ID;
			// Grab Script from contents options table.
			//$CONTENT_OPTION_Query = "SELECT * FROM ContentsOptions Where CONTENT_ID='". $CONTENTID . "'";
			//$CONTENT_OPTION_Code= mysql_query($CONTENT_OPTION_Query);
			//if (mysql_num_rows($CONTENT_OPTION_Code)){
				//while($OPTION_CODE = mysql_fetch_object($CONTENT_OPTION_Code)){
				//	$string .= '**dom**' . $OPTION_CODE->CONTENT_OPTIONS_Script;		
					
				//}
			//}
		}
	}else {
		// URL of 404
		$string = file_get_contents('http://www.ddholdingsinc.com/ContentAI/404.html');
	}	
}else {
	// URL of 404
	$string = file_get_contents('http://www.ddholdingsinc.com/ContentAI/404.html');
}

//echo "Info: ".$WEBID_Array['WEB_Active'].' '.$WEBID_Array['WEB_ID'].$string;
echo $string;

?>