<?php 
require_once("./Connections/ContentConnection.php");
// Specify domains from which requests are allowed
header('Access-Control-Allow-Origin: *');

// Specify which request methods are allowed
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');

/*
 * jQuery < 1.4.0 adds an X-Requested-With header which requires pre-flighting
 * requests. This involves an OPTIONS request before the actual GET/POST to 
 * make sure the client is allowed to send the additional headers.
 * We declare what additional headers the client can send here.
 */

// Additional headers which may be sent along with the CORS request
header('Access-Control-Allow-Headers: X-Requested-With');

// Set the age to 1 day to improve speed/caching.
header('Access-Control-Max-Age: 86400');

// Exit early so the page isn't fully loaded for options requests
if (strtolower($_SERVER['REQUEST_METHOD']) == 'options') {
    exit();
}
if (isset($HTTP_RAW_POST_DATA)) {
    $data = explode('&', $HTTP_RAW_POST_DATA);
    foreach ($data as $val) {
        if (!empty($val)) {
            list($key, $value) = explode('=', $val);   
            $_POST[$key] = urldecode($value);
        }
    }
}
// Create Connection to database
mysql_select_db($database_SrvSite, $SrvSite);

// Set Post Vars as local vars
$pageHost = $_POST['pageHost'];
$pageUrl = $_POST['pageUrl'];
$clientCode = $_POST['clientCode'];
$string = '';



//Set OWA


$OWA_String  = '<script type="text/javascript">';
$OWA_String .= "<![CDATA[ var owa_baseUrl = 'http://james.dealeronlinemarketing.com/owa/'; var owa_cmds = owa_cmds || []; owa_cmds.push(['setSiteId', '";
$OWA_String .= 'fa415655e2949abfde10d573b4f00038';  // SITE ID GOES HERE. //
$OWA_String .= "']); owa_cmds.push(['trackPageView']); owa_cmds.push(['trackClicks']); owa_cmds.push(['trackDomStream']);";
$OWA_String .= "(function() { var _owa = document.createElement('script'); _owa.type = 'text/javascript'; _owa.async = true;	owa_baseUrl = ('https:' == document.location.protocol ? window.owa_baseSecUrl || owa_baseUrl.replace(/http:/, 'https:') : owa_baseUrl );	_owa.src = owa_baseUrl + 'modules/base/js/owa.tracker-combined-min.js'; var _owa_s = document.getElementsByTagName('script')[0]; _owa_s.parentNode.insertBefore(_owa, _owa_s); }()); ]]></script>";


// Build Query for checking referrer url to local database limit by 1 so no dups.
// No website should be in the databse twice

//Execute Query
$WEBID_Query= "SELECT * FROM Websites Where WEB_Url='".$pageHost."' LIMIT 1";
$WEB_IDs = mysql_query($WEBID_Query);

// Build array of data
$WEBID_Array = mysql_fetch_array($WEB_IDs);

// Check for Active Web Else 404 error.
if($WEBID_Array['WEB_Active']==1){
	
	//set vars for checking existing or not and build basic loaders for predefined codes.
	//set vars for global script adding and check length.
	$GoogleUA = $WEBID_Array['WEB_GoogleUACode'];
	$GoogleUA_Universal = $WEBID_Array['WEB_GoogleUA_Universal'];
	$GoogleUA_Universal_Name = $WEBID_Array['WEB_GoogleUA_Universal_Name'];
	//$GoogleWebTools = $WEBID_Array['WEB_GoogleWebToolsMetaCode'];
	//$GooglePlus = $WEBID_Array['WEB_GooglePlusCode'];
	//$BingCode = $WEBID_Array['WEB_BingCode'];
	//$YahooCode = $WEBID_Array['WEB_YahooCode'];
	$GloablScript = $WEBID_Array['WEB_GlobalScript'];
	$CallTrackingCode = $WEBID_Array['WEB_CallTrackingCode'];
	
	
	if($GoogleUA!=''||$GoogleUA!=NULL){
	 $stringGUA = '<script type="text/javascript">';
	 $stringGUA .= 'var _gaq = _gaq || []; ';
	 $stringGUA .= "_gaq.push(['_setAccount', '".$GoogleUA."']); ";
     $stringGUA .= "_gaq.push(['_trackPageview']); ";
	 $stringGUA .= "(function() {var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); })();";
	 $stringGUA .= '</script>';	 
	}
	
	
	if(($GoogleUA_Universal!=''||$GoogleUA_Universal!=NULL)&&($GoogleUA_Universal_Name!=''||$GoogleUA_Universal_Name!=NULL)){
		$stringGUAV = '';
		$stringGUAV = '<script>';
		$stringGUAV .= "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');";
		$stringGUAV .= "ga('create', '".$GoogleUA_Universal."', '".$GoogleUA_Universal_Name."');";
		$stringGUAV .= "ga('send', 'pageview');";
  		$stringGUAV .= "</script>";
	}
	
	
	if($CallTrackingCode!=''||$CallTrackingCode!=NULL){
		$stringCT = "";
		$stringCT = '<!-- start number replacer --><script type="text/javascript"><!--vs_account_id      = "';
		$stringCT .= $CallTrackingCode;
		$stringCT .= '"; //--></script><script type="text/javascript" src="http://marchex.voicestar.com/euinc/number-changer.js"></script><!-- end ad widget -->';
	}	
	
	//$string = 'UA '.$GoogleUA.' WT '.$GoogleWebTools.' GP '.$GooglePlus.' BC '.$BingCode.' YC '.$YahooCode.' GS '.$GloablScript.';';
	$string = $stringGUA.' '.$stringGUAV.' '.$stringCT.' '.$OWA_String;
	
}else{ $string = ''; }

//Return String of codes back to site.

echo $string;



//Set Crazy Egg Script Vars Must be two vars for line return to work.

$C_egg1  = '<script type="text/javascript">';
$C_egg1 .= 'setTimeout(function(){var a=document.createElement("script");var b=document.getElementsByTagName("script")[0];a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0013/0674.js?"+Math.floor(new Date().getTime()/3600000);';
$C_egg2  = 'a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b);});';
$C_egg2 .= '</script>';

echo $C_egg1;
?>
<br/>
<?php
echo $C_egg2;

?>