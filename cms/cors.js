function postCORS(url, data, callback, type)
{
	if (jQuery.browser.msie && window.XDomainRequest) {
        // Use XDR
		var params = '&clientCode='+data.clientCode+'&pageUrl='+data.pageUrl+'&pageHost='+data.pageHost;
        var xdr = new XDomainRequest();
        xdr.open("post", url);
        setTimeout(function(){
			xdr.send(params);
		}, 0);
        xdr.onload = function() {
            callback(handleXDROnload(this, type));
        };
		xdr.onprogress = function(){};
		xdr.ontimeout = function(){};
		xdr.onerror = function(){};
    } else if (!jQuery.browser.msie) {
		jQuery.post(url, data, callback, type);
	} else {
		var output = '<table width="600" border="0" cellspacing="0" cellpadding="0"><tr><td align="center" style="font-family:Arial, Helvetica, sans-serif; padding:10px;"><h1>Browser Not Supported</h1></td></tr><tr><td align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:16px; padding:10px;">This content requires <a href="http://windows.microsoft.com/en-us/internet-explorer/products/ie/home/">Internet Explorer 8+</a>, <a href="http://www.mozilla.org/en-US/firefox/new/">Firefox 3.5+</a>, <a href="http://www.apple.com/safari/download/">Safari 4+</a>, or <a href="https://www.google.com/intl/en/chrome/browser/">Google Chrome</a>.<br /><br />Please update your browser to view this content.</td></tr></table>';
		callback(output);
	}
}
function handleXDROnload(xdr, type)
{
    var responseText = xdr.responseText, dataType = type || "";
    if (dataType.toLowerCase() == "xml"
        && typeof responseText == "string") {
        // If expected data type is xml, we need to convert it from a
        // string to an XML DOM object
        var doc;
        try {
            if (window.ActiveXObject) {
                doc = new ActiveXObject('Microsoft.XMLDOM');
                doc.async = 'false';
				console.log(responseText);
                doc.loadXML(responseText);
            } else {
                var parser = new DOMParser();
                doc = parser.parseFromString(responseText, 'text/xml');
            }
            return doc;
        } catch(e) {
            // ERROR parsing XML for conversion, just return the responseText
        }
    }
    return responseText;
}
