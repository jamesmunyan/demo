<?php

define('Testing_PATH', '/mnt/stor08-wc1-ord1/718973/testing.dealeronlinemarketing.com/web/content/logs/');

define('Content_PATH', '/mnt/stor08-wc1-ord1/718973/content.dealeronlinemarketing.com/web/content/logs/');

define('DOM_PATH', '/mnt/stor08-wc1-ord1/718973/www.dom360.com/web/content/logs/');

function destroy($dir) {
    $mydir = opendir($dir);
    while(false !== ($file = readdir($mydir))) {
        if($file != "." && $file != "..") {
            chmod($dir.$file, 0777);
            if(is_dir($dir.$file)) {
                chdir('.');
                destroy($dir.$file.'/');
                rmdir($dir.$file) or DIE("couldn't delete $dir$file\r\n\r\n");
            }
            else
                unlink($dir.$file) or DIE("couldn't delete $dir$file\r\n\r\n");
        }
    }
    closedir($mydir);
}

echo "Delete Testing logs.\r\n\r\n";
destroy(Testing_PATH);
echo "Finished\r\n\r\n";
echo "Delete Content logs.\r\n\r\n";
destroy(Content_PATH);
echo "Finished\r\n\r\n";
echo "Delete Live logs.\r\n\r\n";
destroy(DOM_PATH);
echo "Finished\r\n\r\n";

?>