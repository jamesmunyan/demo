<?php 
require_once("Connections/ContentConnection.php");
// Specify domains from which requests are allowed
header('Access-Control-Allow-Origin: *');

// Specify which request methods are allowed
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
/*
 * jQuery < 1.4.0 adds an X-Requested-With header which requires pre-flighting
 * requests. This involves an OPTIONS request before the actual GET/POST to 
 * make sure the client is allowed to send the additional headers.
 * We declare what additional headers the client can send here.
 */

// Additional headers which may be sent along with the CORS request
header('Access-Control-Allow-Headers: X-Requested-With');

// Set the age to 1 day to improve speed/caching.
header('Access-Control-Max-Age: 86400');

// Exit early so the page isn't fully loaded for options requests
if (strtolower($_SERVER['REQUEST_METHOD']) == 'options') {
    exit();
}
if (isset($HTTP_RAW_POST_DATA)) {
    $data = explode('&', $HTTP_RAW_POST_DATA);
    foreach ($data as $val) {
        if (!empty($val)) {
            list($key, $value) = explode('=', $val);   
            $_POST[$key] = urldecode($value);
        }
    }
}
// Create Connection to database
mysql_select_db($database_SrvSite, $SrvSite);

// Set Post Vars as local vars
$pageHost = $_POST['pageHost'];
$pageUrl = $_POST['pageUrl'];
//$clientCode = $_POST['clientCode'];
$string = 'test';

// Build Query for checking referrer url to local database limit by 1 so no dups.
// No website should be in the database twice

//Execute Query
$WEBID_Query= "SELECT * FROM Websites Where WEB_Url='".$pageHost."' LIMIT 1";
$WEB_IDs = mysql_query($WEBID_Query);

// Build array of data
$WEBID_Array = mysql_fetch_array($WEB_IDs);

// Check for Active Web Else 404 error.
if($WEBID_Array['WEB_Active']==1){
	
	//set vars for checking existing or not and build basic loaders for predefined codes.
	//set vars for global script adding and check length.
	$GoogleUA = $WEBID_Array['WEB_GoogleUACode'];
	$GoogleUA_Universal = $WEBID_Array['WEB_GoogleUA_Universal'];
	$GoogleUA_Universal_Name = $WEBID_Array['WEB_GoogleUA_Universal_Name'];
	
	
	if($GoogleUA!=''||$GoogleUA!=NULL){
	 //$stringGUA = '<script id="dom360_gua" type="text/javascript">';
	 $stringGUA = 's';
	 $stringGUA .= 'var _gaq = _gaq || [];';
	 $stringGUA .= "\n_gaq.push(['_setAccount', '".$GoogleUA."']); \n";
     $stringGUA .= "_gaq.push(['_trackPageview']); \n";
	 $stringGUA .= "(function() \n{var ga = document.createElement('script'); \nga.type = 'text/javascript'; \nga.async = true; \nga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js'; \nvar s = document.getElementsByTagName('script')[0]; \ns.parentNode.insertBefore(ga, s); \n})();";
	 //$stringGUA .= '</script>';	 
	}
	
	
	if(($GoogleUA_Universal!=''||$GoogleUA_Universal!=NULL)&&($GoogleUA_Universal_Name!=''||$GoogleUA_Universal_Name!=NULL)){
		$stringGUAV = 's';
		//$stringGUAV .= '<script>';
		$stringGUAV .= "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');";
		$stringGUAV .= "ga('create', '".$GoogleUA_Universal."', '".$GoogleUA_Universal_Name."');";
		$stringGUAV .= "ga('send', 'pageview');";
  		//$stringGUAV .= "</script>";
	}
	
	$string = $stringGUA.'|:|'.$stringGUAV;
	
}else{ /*Website is not active so do not return any scripts*/
	$string = '';
}

//Return String
echo $string;
?>