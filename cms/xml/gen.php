<?php
function curPageURL() {
 $pageURL = 'http';
 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";
 if ($_SERVER["SERVER_PORT"] != "80") {
  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 }
 return $pageURL;
}
?>
<?php
$cc = -1;
$type = -1;
//echo 'site: '.$_SERVER['HTTP_REFERER'].' ->>';
//$currentpage = curPageURL();

$currentpage = explode('?', $_SERVER['HTTP_REFERER']);


if(isset($_GET['CC'])){
	$cc = $_GET['CC'];
}
if(isset($_GET['Type'])){
	$type = $_GET['Type'];
}

if($cc == 'DDCT' && $type=='xml'){ 
	header('Content-type: text/xml');
	header('Pragma: public');
	header('Cache-control: private');
	header('Expires: -1');
	$xmldata = '<?xml version="1.0" encoding="UTF-8"?>';
		$xmldata .= '<rss version="2.0">';
			$xmldata .= '<channel>';
				
				$xmldata .= '<title>';
				$xmldata .= 'Title of Page';
				$xmldata .= '</title>';
				
				$xmldata .= '<link>';
				$xmldata .= 'http://domtestaccount.cms.dealer.com';
				$xmldata .= '</link>';
				
				$xmldata .= '<description>';
				$xmldata .= 'Meta Data';
				$xmldata .= '</description>';
				
				$xmldata .= '<item>';
				
					$xmldata .= '<title>';
					$xmldata .= 'Page Header';
					$xmldata .= '</title>';
					
					$xmldata .= '<link>';
					$xmldata .= 'http://domtestaccount.cms.dealer.com';
					$xmldata .= '</link>';
					
					$xmldata .= '<description>';
					$xmldata .= 'Code for page of '. $currentpage[0];
					$xmldata .= '</description>';
					
				$xmldata .= '</item>';
				
			$xmldata .= '</channel>';
		$xmldata .= '</rss>';
}

echo $xmldata;
?>