<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>World Toyota</title>
</head>
<style>
	body, DomWrapper {
		background: #495867;
		font-family: Helvetica, Arial, sans-serif;
		color:#FFF;
	}
	table, tr, td {
		margin: 0px;
		padding: 0px;
	}
	img {
		border: none;
		margin: 0px;
		padding: 0px;
	}
	label {
		font-weight:bold;
	}
	#DomWrapper {
		width: 700px;
	}
	.right_top {
		padding-left: 10px;
		padding-bottom: 10px;
		padding-top: 0px;
	}
	.thirteen {
		padding: 10px;
	}
	.label_left {
		padding-left: 10px;
		padding-bottom: 2px;
		padding-top: 2px;
	}
	.DomSubmit {
		
		width:187px;
		height:40px;
		border:none;
		opacity: 0;
	}
	.DomFields {
		padding-left: 15px;
	}
	.DomSubmitDiv {
		padding-top: 5px;
		background: no-repeat url(images/submit.png);
		background-position: center;
	}
	.checkbox{
	-moz-border-radius: 2px; 
	-webkit-border-radius: 2px;
	border-radius: 2px;
	}
.formfields{
	-moz-border-radius: 2px; 
	-webkit-border-radius: 2px;
	border-radius: 2px;
	}
</style>
<script language="javascript">
	function validateForm()
	{
		var x=document.forms["still_searching"]["_custom=FIELD_MAKE_1359647619517=3=Make"].value;
		if (x==null || x=="")
		{
			alert("Make name must be filled out");
			return false;
  		}
		
		var x=document.forms["still_searching"]["_custom=FIELD_TEXT_1359647607810=4=Model"].value;
		if (x==null || x=="")
		{
			alert("Model must be filled out");
			return false;
  		}
		
		
		var x=document.forms["still_searching"]["contact.firstName.sep"].value;
		if (x==null || x=="")
		{
			alert("First name must be filled out");
			return false;
  		}
		
		var x=document.forms["still_searching"]["contact.lastName.sep"].value;
		if (x==null || x=="")
		{
			alert("Last name must be filled out");
			return false;
  		}
		
		var x=document.forms["still_searching"]["contact.phone.sep"].value;
		if (x==null || x=="")
		{
		alert("Phone must be filled out");
		return false;
		}
  
		var x=document.forms["still_searching"]["contact.email.sep"].value;
		var atpos=x.indexOf("@");
		var dotpos=x.lastIndexOf(".");
		if (x==null || x=="")
		{
		  alert("Email must be filled out");
		  return false;
		}
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
		{
		  alert("Not a valid e-mail address");
		  return false;
		}
	};
</script>

<body>
<div id="DomWrapper">
<table width="700" border="0" cellpadding="0">
	<tr>
		<td width="227"><img src="images/WOTO-1196-JanuaryPopUp_01.png" width="227" height="136" /></td>
		<td><img src="images/WOTO-1196-JanuaryPopUp_02.png" width="473" height="136" /></td>
	</tr>
</table>
<table width="700" border="0" cellpadding="0">
	<tr>
		<td><img src="images/WOTO-1196-JanuaryPopUp_03.png" width="700" height="56" /></td>
	</tr>
</table>
<form name="still_searching" action="http://www.worldtoyota.com/form/confirm.htm?pageAlias=SITEBUILDER_POPOVER_STILL_SEARCHING_1&amp;formId=worldtoyotag1a_contact_4" method="post" class="validate " onSubmit="return validateForm()">
<input type="hidden" name="accountId" class="hidden" value="worldtoyotag1a" />
<table width="700" border="0" cellpadding="0">
	<tr>
		<td width="342" valign="top">
			<table width="342" border="0" cellpadding="0">
				<!-- make -->
				<tr><td class="label_left"><label>Make*</label></td><td class="DomFields"><input class="formfields" type="text" name="_custom=FIELD_MAKE_1359647619517=3=Make" tabindex="3" required="required" pattern="(^$)|(^.*(\S+).*$)" /></td></tr>
				<!-- model -->
				<tr><td class="label_left"><label>Model*</label></td><td class="DomFields"><input class="formfields" type="text" name="_custom=FIELD_TEXT_1359647607810=4=Model" tabindex="4" required="required" pattern="(^$)|(^.*(\S+).*$)" /></td></tr>
				<!-- first -->
				<tr><td class="label_left"><label>First Name*</label></td><td class="DomFields"><input class="formfields" type="text" name="contact.firstName" tabindex="5" required="required" pattern="(^$)|(^[a-zA-Z][-a-zA-Z.' ]{1,}$)" /></td></tr>
				<!-- last -->
				<tr><td class="label_left"><label>Last Name*</label></td><td class="DomFields"><input class="formfields" type="text" name="contact.lastName" tabindex="6" required="required" pattern="(^$)|(^[a-zA-Z][-a-zA-Z.' ]{1,}$)" /></td></tr>
				<!-- email -->
				<tr><td class="label_left"><label>Email*</label></td><td class="DomFields"><input class="formfields" type="email" name="contact.email.sep" tabindex="7" required="required" pattern="(^$)|(^.*(\S+).*$)" /></td></tr>
				<!-- phone -->
				<tr><td class="label_left"><label>Phone*</label></td><td class="DomFields"><input class="formfields" type="text" name="contact.phone.sep" tabindex="8" required="required" pattern="(^$)|(^\+?[1]?[-. ]?\(?[\d]{3}\)?[-. ]?[\d]{3}[-. ]?[\d]{4}[ a-zA-Z0-9.:]*$)" /></td></tr>
			</table>
			<table width="342" border="0" cellpadding="0">
				<!-- required -->
				<tr><td class="thirteen">*REQUIRED FIELD</td></tr>
				<!-- button -->
				<tr><td align="center" class="DomSubmitDiv"><button type="submit" class="DomSubmit"><img src="images/spacer.gif" width="187" height="41" /></button></td></tr>
			</table>
		</td>
		<td width="358">
			<table width="358" border="0" cellpadding="0">
				<tr>
					<td class="right_top"><strong>Select one:</strong>&nbsp;&nbsp;<label><input id="newcar" type="checkbox" name="_custom=FIELD_CHECKBOXGROUP_1359649800229=2=Select one:" value="New" tabindex="2" data-pre--owned="FIELD_OPTION_1359649800231" data-new="FIELD_OPTION_1359649800230" onclick="SingleSelect('chk',this);" />New</label>&nbsp;&nbsp;<label>
<input id="preowned" type="checkbox" name="_custom=FIELD_CHECKBOXGROUP_1359649800229=2=Select one:" value="Pre-Owned" tabindex="2" data-pre--owned="FIELD_OPTION_1359649800231" data-new="FIELD_OPTION_1359649800230" onclick="SingleSelect('chk',this);" />Pre-Owned</label></td>
				</tr>
				<tr>
					<td><img src="images/WOTO-1196-JanuaryPopUp_06.png" width="358" height="203" alt=""></td>	
				</tr>
			</table>

		</td>
	</tr>
</table>
<input type="hidden" name="v8form" class="hidden" value="true"  />
<input type="hidden" name="userSessionId" class="hidden" value="90eb3b910a0a000707a98dcbca9ebc55"  />
<input type="hidden" name="form.id" class="hidden" value="CONTACT"  />
<input type="hidden" name="confirmation" class="hidden" value="CONFIRM_DEFAULT"  />
<input type="hidden" name="_action" class="hidden" value="SubmitGenericLead"  />
<input type="hidden" name="portalId" class="hidden"  />
<input type="hidden" name="source" class="hidden" value="popover-still-searching - Dealer.Com Website"  />
<input type="hidden" name="vk" class="hidden" value="5r4n8l5g"  />
<input type="hidden" name="vv" class="hidden" value="5r4n8l5g"  />
</form>

<script language="javascript">

	var NewcarCheck, backCheck;

onload = function() {
    NewcarCheck = document.forms[0].newcar;
    PreownedCheck = document.forms[0].preowned;
    NewcarCheck.onclick = checkClick;
    PreownedCheck.onclick = checkClick;
};

function checkClick(e) {
    var otherCheckbox = this === NewcarCheck ? PreownedCheck : NewcarCheck;
    if (this.checked) {
        otherCheckbox.checked = false;
    }
}
</script>

</div>
</body>
</html>
