function DOM_CreateCookie(name,value,days) {
	// new Date object created
	var date = new Date();
	// adding days to current time
	date.setTime(date.getTime()+(days*24*60*60*1000));
	// converting to standard format
	var expires = date.toGMTString(); 
	// adding cookie name,value and expiry time
	document.cookie = name+"="+value+"; expires="+expires+"; path=/";
}

function DOM_ReadCookie(name) {
	var flag = 0;
	// getting the browser cookie values into an array
	var dcmntCookie = document.cookie.split(';');
	// looping over the array to find our cookie
	for(var i=0;i < dcmntCookie.length;i++) {
		var ck = dcmntCookie[i];
		// loop for removing extra spaces from the beginning of each cookie
		while (ck.charAt(0)==' ') {
			ck = ck.substring(1,ck.length);
		}
		if(ck) {
			// splitting the cookie into its name and value
			cparts = ck.split('=');
			// setting the flag if a cookie with the name specified exists
			if (cparts[0]==name)
			flag=1;
		}              
	}    
	// returning true if cookie exists else returning false 
	if(flag) { 
		return true; 
	} else {
		return false; 
	}   
}
			
function DOM_CheckCookie(name) {
	if (DOM_ReadCookie(name)) {
		document.getElementById('DomPopWrapper').style.display = "none";
		document.getElementById('DomPopWrapper').style.visibility = "hidden";
	}
	else DOM_CreateCookie(name,"Dealer Online Marketing",1); 
}

function DOM_ClosePop() {
	document.getElementById('DomPopWrapper').style.display = "none";
	document.getElementById('DomPopWrapper').style.visibility = "hidden";
}