function validateForm() {
	var x=document.forms["still_searching"]["_custom=FIELD_TEXT_1379959006685=2=Make"].value;
	if (x==null || x=="")
	{
		alert("Make name must be filled out");
		return false;
	}
	
	var x=document.forms["still_searching"]["_custom=FIELD_TEXT_1379959013436=3=Model"].value;
	if (x==null || x=="")
	{
		alert("Model must be filled out");
		return false;
	}
	
	
	var x=document.forms["still_searching"]["contact.firstName.sep"].value;
	if (x==null || x=="")
	{
		alert("First name must be filled out");
		return false;
	}
	
	var x=document.forms["still_searching"]["contact.lastName.sep"].value;
	if (x==null || x=="")
	{
		alert("Last name must be filled out");
		return false;
	}
	
	var x=document.forms["still_searching"]["contact.phone.sep"].value;
	if (x==null || x=="")
	{
	alert("Phone must be filled out");
	return false;
	}
	
	var x=document.forms["still_searching"]["contact.email.sep"].value;
	var atpos=x.indexOf("@");
	var dotpos=x.lastIndexOf(".");
	if (x==null || x=="")
	{
	  alert("Email must be filled out");
	  return false;
	}
	if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
	{
	  alert("Not a valid e-mail address");
	  return false;
	}
};