<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Install_admin_tools_permissions extends Migration {

	// permissions to migrate
	private $permission_values = array(
		array('name' => 'Admin_Tools.Content.View', 'description' => '', 'status' => 'active',),
		array('name' => 'Admin_Tools.Content.Create', 'description' => '', 'status' => 'active',),
		array('name' => 'Admin_Tools.Content.Edit', 'description' => '', 'status' => 'active',),
		array('name' => 'Admin_Tools.Content.Delete', 'description' => '', 'status' => 'active',),
		array('name' => 'Admin_Tools.Reports.View', 'description' => '', 'status' => 'active',),
		array('name' => 'Admin_Tools.Reports.Create', 'description' => '', 'status' => 'active',),
		array('name' => 'Admin_Tools.Reports.Edit', 'description' => '', 'status' => 'active',),
		array('name' => 'Admin_Tools.Reports.Delete', 'description' => '', 'status' => 'active',),
		array('name' => 'Admin_Tools.Settings.View', 'description' => '', 'status' => 'active',),
		array('name' => 'Admin_Tools.Settings.Create', 'description' => '', 'status' => 'active',),
		array('name' => 'Admin_Tools.Settings.Edit', 'description' => '', 'status' => 'active',),
		array('name' => 'Admin_Tools.Settings.Delete', 'description' => '', 'status' => 'active',),
		array('name' => 'Admin_Tools.Developer.View', 'description' => '', 'status' => 'active',),
		array('name' => 'Admin_Tools.Developer.Create', 'description' => '', 'status' => 'active',),
		array('name' => 'Admin_Tools.Developer.Edit', 'description' => '', 'status' => 'active',),
		array('name' => 'Admin_Tools.Developer.Delete', 'description' => '', 'status' => 'active',),
	);

	//--------------------------------------------------------------------

	public function up()
	{
		$prefix = $this->db->dbprefix;

		// permissions
		foreach ($this->permission_values as $permission_value)
		{
			$permissions_data = $permission_value;
			$this->db->insert("permissions", $permissions_data);
			$role_permissions_data = array('role_id' => '1', 'permission_id' => $this->db->insert_id(),);
			$this->db->insert("role_permissions", $role_permissions_data);
		}
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$prefix = $this->db->dbprefix;

        // permissions
		foreach ($this->permission_values as $permission_value)
		{
			$query = $this->db->select('permission_id')->get_where("permissions", array('name' => $permission_value['name'],));
			foreach ($query->result_array() as $row)
			{
				$permission_id = $row['permission_id'];
				$this->db->delete("role_permissions", array('permission_id' => $permission_id));
			}
			$this->db->delete("permissions", array('name' => $permission_value['name']));

		}
	}

	//--------------------------------------------------------------------

}