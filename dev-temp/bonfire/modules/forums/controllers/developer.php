<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class developer extends Admin_Controller {

	//--------------------------------------------------------------------


	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict('Forums.Developer.View');
		$this->load->model('forums_model', null, true);
		$this->lang->load('forums');
		
		Template::set_block('sub_nav', 'developer/_sub_nav');
	}

	//--------------------------------------------------------------------



	/*
		Method: index()

		Displays a list of form data.
	*/
	public function index()
	{

		// Deleting anything?
		if (isset($_POST['delete']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = $this->forums_model->delete($pid);
				}

				if ($result)
				{
					Template::set_message(count($checked) .' '. lang('forums_delete_success'), 'success');
				}
				else
				{
					Template::set_message(lang('forums_delete_failure') . $this->forums_model->error, 'error');
				}
			}
		}

		$records = $this->forums_model->find_all();

		Template::set('records', $records);
		Template::set('toolbar_title', 'Manage Forums');
		Template::render();
	}

	//--------------------------------------------------------------------



	/*
		Method: create()

		Creates a Forums object.
	*/
	public function create()
	{
		$this->auth->restrict('Forums.Developer.Create');

		if ($this->input->post('save'))
		{
			if ($insert_id = $this->save_forums())
			{
				// Log the activity
				$this->activity_model->log_activity($this->current_user->id, lang('forums_act_create_record').': ' . $insert_id . ' : ' . $this->input->ip_address(), 'forums');

				Template::set_message(lang('forums_create_success'), 'success');
				Template::redirect(SITE_AREA .'/developer/forums');
			}
			else
			{
				Template::set_message(lang('forums_create_failure') . $this->forums_model->error, 'error');
			}
		}
		Assets::add_module_js('forums', 'forums.js');

		Template::set('toolbar_title', lang('forums_create') . ' Forums');
		Template::render();
	}

	//--------------------------------------------------------------------



	/*
		Method: edit()

		Allows editing of Forums data.
	*/
	public function edit()
	{
		$id = $this->uri->segment(5);

		if (empty($id))
		{
			Template::set_message(lang('forums_invalid_id'), 'error');
			redirect(SITE_AREA .'/developer/forums');
		}

		if (isset($_POST['save']))
		{
			$this->auth->restrict('Forums.Developer.Edit');

			if ($this->save_forums('update', $id))
			{
				// Log the activity
				$this->activity_model->log_activity($this->current_user->id, lang('forums_act_edit_record').': ' . $id . ' : ' . $this->input->ip_address(), 'forums');

				Template::set_message(lang('forums_edit_success'), 'success');
			}
			else
			{
				Template::set_message(lang('forums_edit_failure') . $this->forums_model->error, 'error');
			}
		}
		else if (isset($_POST['delete']))
		{
			$this->auth->restrict('Forums.Developer.Delete');

			if ($this->forums_model->delete($id))
			{
				// Log the activity
				$this->activity_model->log_activity($this->current_user->id, lang('forums_act_delete_record').': ' . $id . ' : ' . $this->input->ip_address(), 'forums');

				Template::set_message(lang('forums_delete_success'), 'success');

				redirect(SITE_AREA .'/developer/forums');
			} else
			{
				Template::set_message(lang('forums_delete_failure') . $this->forums_model->error, 'error');
			}
		}
		Template::set('forums', $this->forums_model->find($id));
		Assets::add_module_js('forums', 'forums.js');

		Template::set('toolbar_title', lang('forums_edit') . ' Forums');
		Template::render();
	}

	//--------------------------------------------------------------------


	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/*
		Method: save_forums()

		Does the actual validation and saving of form data.

		Parameters:
			$type	- Either "insert" or "update"
			$id		- The ID of the record to update. Not needed for inserts.

		Returns:
			An INT id for successful inserts. If updating, returns TRUE on success.
			Otherwise, returns FALSE.
	*/
	private function save_forums($type='insert', $id=0)
	{
		if ($type == 'update') {
			$_POST['id'] = $id;
		}

		
		$this->form_validation->set_rules('forums_FORUM_Title','forum title','required|unique[bf_forums.forums_FORUM_Title,bf_forums.id]|max_length[255]');

		if ($this->form_validation->run() === FALSE)
		{
			return FALSE;
		}

		// make sure we only pass in the fields we want
		
		$data = array();
		$data['forums_FORUM_Title']        = $this->input->post('forums_FORUM_Title');

		if ($type == 'insert')
		{
			$id = $this->forums_model->insert($data);

			if (is_numeric($id))
			{
				$return = $id;
			} else
			{
				$return = FALSE;
			}
		}
		else if ($type == 'update')
		{
			$return = $this->forums_model->update($id, $data);
		}

		return $return;
	}

	//--------------------------------------------------------------------



}