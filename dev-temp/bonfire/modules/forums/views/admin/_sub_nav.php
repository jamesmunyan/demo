<ul class="nav nav-pills">
	<li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>
		<a href="<?php echo site_url(SITE_AREA .'/admin/forums') ?>" id="list"><?php echo lang('forums_list'); ?></a>
	</li>
	<?php if ($this->auth->has_permission('Forums.Admin.Create')) : ?>
	<li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?> >
		<a href="<?php echo site_url(SITE_AREA .'/admin/forums/create') ?>" id="create_new"><?php echo lang('forums_new'); ?></a>
	</li>
	<?php endif; ?>
</ul>