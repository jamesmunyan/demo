<ul class="nav nav-pills">
	<li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>
		<a href="<?php echo site_url(SITE_AREA .'/content/forums') ?>" id="list"><?php echo lang('forums_list'); ?></a>
	</li>
	<?php if ($this->auth->has_permission('Forums.Content.Create')) : ?>
	<li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?> >
		<a href="<?php echo site_url(SITE_AREA .'/content/forums/create') ?>" id="create_new"><?php echo lang('forums_new'); ?></a>
	</li>
	<?php endif; ?>
</ul>