
<?php if (validation_errors()) : ?>
<div class="alert alert-block alert-error fade in ">
  <a class="close" data-dismiss="alert">&times;</a>
  <h4 class="alert-heading">Please fix the following errors :</h4>
 <?php echo validation_errors(); ?>
</div>
<?php endif; ?>
<?php // Change the css classes to suit your needs
if( isset($forums) ) {
    $forums = (array)$forums;
}
$id = isset($forums['id']) ? $forums['id'] : '';
?>
<div class="admin-box">
    <h3>Forums</h3>
<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
    <fieldset>
        <div class="control-group <?php echo form_error('forums_FORUM_Title') ? 'error' : ''; ?>">
            <?php echo form_label('forum title'. lang('bf_form_label_required'), 'forums_FORUM_Title', array('class' => "control-label") ); ?>
            <div class='controls'>
        <input id="forums_FORUM_Title" type="text" name="forums_FORUM_Title" maxlength="255" value="<?php echo set_value('forums_FORUM_Title', isset($forums['forums_FORUM_Title']) ? $forums['forums_FORUM_Title'] : ''); ?>"  />
        <span class="help-inline"><?php echo form_error('forums_FORUM_Title'); ?></span>
        </div>


        </div>



        <div class="form-actions">
            <br/>
            <input type="submit" name="save" class="btn btn-primary" value="Create Forums" />
            or <?php echo anchor(SITE_AREA .'/developer/forums', lang('forums_cancel'), 'class="btn btn-warning"'); ?>
            
        </div>
    </fieldset>
    <?php echo form_close(); ?>


</div>
