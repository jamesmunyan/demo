<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
   	<meta name="google-site-verification" content="XfhfXepHLQ869Ug0MCIcvjI43UIGCbW9SXgUxkzNd6g" />
	<link href="css/screen.css" rel="stylesheet" type="text/css" media="screen" />
	<script type="text/javascript">document.write(unescape("%3Cscript src='" + (("https:" == document.location.protocol) ? "https" : "http") + "://c.mouseflow.com/projects/73353ce5-9eea-4e6a-8c10-0e57ded5cdad.js' type='text/javascript'%3E%3C/script%3E"));</script>
    
    <title>Internet Marketing Solutions for Auto Dealers | Auto Marketing Firm</title>
    <meta name="keywords" content="Internet Marketing Solutions for Auto Dealers, Auto Marketing Firm, Automotive SEO" />
    <meta name="description" content="As a leading auto marketing firm, Dealer Online Marketing offers Internet marketing solutions for auto dealers. Get a free online presence analysis today." />
    
</head>

<body>

	<div id="wrapper">
		
		<div id="sidebar">
			<?php $current = 2; include('sidebar.php'); ?>
            
		</div>

		<div id="main">
        
		<div class="headerImg"><img src="images/aboutHeader.jpg" alt="About Us Header" /></div>

	        <h1>Born Out of the Recession</h1>
      
			<p>Dealer Online Marketing is a new-age marketing firm that utilizes the Internet to help you become more successful. A talented staff of copywriters, web designers, vendor managers, graphic artists, HTML coders, and more are available to help you stay on the cutting edge of the automotive 				industry. At Dealer Online Marketing each project is deemed a priority with a quick turnaround to help you get what you want when you need it. Whether you are requesting vehicle descriptions, a sales e-blast, or a website correction, your needs are our number-one concern. Please 			
            	contact us today: 1-800-989-0298.</p><br />
            
            <p>
            <ul>
            	<li><span style="color: #ed1c24; font-size: 90%; font-weight: bold; text-transform: uppercase;">What exactly does Dealer Online Marketing do?</span><br />
                	On a macro level, we manage the entire online presence of your dealership to increase your sales. We deal with all the facets of your inventory, your Web site, your third-party sites, your social sites, and more. Simply put we take the existing technology you already have in place, build on it, perfect it, and then push it to its maximum limits so you’re getting the most return possible.</li>

				<li><span style="color: #ed1c24; font-size: 90%; font-weight: bold; text-transform: uppercase;">As a dealer, what's in it for me?</span><br />
                	Sales! With Dealer Online Marketing driving high-quality organic phone, walk-in, and internet traffic to your dealership, you will see an increase in sales within the first 90 days and a steady increase thereafter &mdash; all our clients receive thousands and thousands of unique visitors to their Web sites each and every month. You will also get grade-A web design, beautiful vehicle descriptions, and the best, promptest customer service around. Lastly you will save money; you basically have nothing to lose and everything to gain!</li>

				<li><span style="color: #ed1c24; font-size: 90%; font-weight: bold; text-transform: uppercase;">How much time will Dealer Online Marketing take out of my day?</span><br />
                	While phone and e-mail interaction takes place every day, your time spent dealing with us will take less than 30 minutes of your time a week.</li>

				<li><span style="color: #ed1c24; font-size: 90%; font-weight: bold; text-transform: uppercase;">Will Dealer Online Marketing’s designs mesh with the designs of my traditional advertising?</span><br />
                	Yes. The best case scenario is to let Dealer Online Marketing handle the entire marketing strategy &mdash; interactive and traditional. That way the overall look and feel of your traditional and online advertising will be seamless.</li>
            </ul>
            
            
            

	</div>

</body>
</html>
