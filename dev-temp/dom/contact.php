<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="google-site-verification" content="XfhfXepHLQ869Ug0MCIcvjI43UIGCbW9SXgUxkzNd6g" />
	<link href="css/screen.css" rel="stylesheet" type="text/css" media="screen" />
  	<script type="text/javascript">document.write(unescape("%3Cscript src='" + (("https:" == document.location.protocol) ? "https" : "http") + "://c.mouseflow.com/projects/73353ce5-9eea-4e6a-8c10-0e57ded5cdad.js' type='text/javascript'%3E%3C/script%3E"));</script>
  
    <title>Internet Marketing Solutions for Auto Dealers | Automotive SEO</title>
    <meta name="keywords" content="Internet Marketing Solutions for Auto Dealers, Auto Marketing Firm, Automotive SEO" />
    <meta name="description" content="Dealer Online Marketing offers Internet marketing solutions for auto dealers like automotive SEO and more. Contact us for a free online presence analysis." />
    
    
    <script type="text/javascript">
function validateFormOnSubmit(theForm) {
var reason = "";

  reason += validateEmpty(theForm.first_name);
  reason += validateEmpty(theForm.last_name);
  reason += validateEmpty(theForm.company);
  reason += validateEmpty(theForm.email);
  reason += validateEmail(theForm.email);
  reason += validatePhone(theForm.phone);
  reason += validatePhone(theForm.mobile);
  
      
  if (reason != "") {
    alert("Some fields need correction:\n" + reason);
    return false;
  }

  return true;
}
function validateEmpty(fld) {
    var error = "";
 
    if (fld.value.length == 0) {
        fld.style.background = 'Yellow'; 
        error = "The required field has not been filled in.\n"
    } else {
        fld.style.background = 'White';
    }
    return error;  
}
function validateUsername(fld) {
    var error = "";
    var illegalChars = /\W/; // allow letters, numbers, and underscores
 
    if (fld.value == "") {
        fld.style.background = 'Yellow'; 
        error = "You didn't enter a username.\n";
    } else if ((fld.value.length < 5) || (fld.value.length > 15)) {
        fld.style.background = 'Yellow'; 
        error = "The username is the wrong length.\n";
    } else if (illegalChars.test(fld.value)) {
        fld.style.background = 'Yellow'; 
        error = "The username contains illegal characters.\n";
    } else {
        fld.style.background = 'White';
    }
    return error;
}
function validatePassword(fld) {
    var error = "";
    var illegalChars = /[\W_]/; // allow only letters and numbers 
 
    if (fld.value == "") {
        fld.style.background = 'Yellow';
        error = "You didn't enter a password.\n";
    } else if ((fld.value.length < 7) || (fld.value.length > 15)) {
        error = "The password is the wrong length. \n";
        fld.style.background = 'Yellow';
    } else if (illegalChars.test(fld.value)) {
        error = "The password contains illegal characters.\n";
        fld.style.background = 'Yellow';
    } else if (!((fld.value.search(/(a-z)+/)) && (fld.value.search(/(0-9)+/)))) {
        error = "The password must contain at least one numeral.\n";
        fld.style.background = 'Yellow';
    } else {
        fld.style.background = 'White';
    }
   return error;
}  
function trim(s)
{
  return s.replace(/^\s+|\s+$/, '');
}
function validateEmail(fld) {
    var error="";
    var tfld = trim(fld.value);                        // value of field with whitespace trimmed off
    var emailFilter = /^[^@]+@[^@.]+\.[^@]*\w\w$/ ;
    var illegalChars= /[\(\)\<\>\,\;\:\\\"\[\]]/ ;
   
    if (fld.value == "") {
        fld.style.background = 'Yellow';
        error = "You didn't enter an email address.\n";
    } else if (!emailFilter.test(tfld)) {              //test email for illegal characters
        fld.style.background = 'Yellow';
        error = "Please enter a valid email address.\n";
    } else if (fld.value.match(illegalChars)) {
        fld.style.background = 'Yellow';
        error = "The email address contains illegal characters.\n";
    } else {
        fld.style.background = 'White';
    }
    return error;
}
function validatePhone(fld) {
    var error = "";
    var stripped = fld.value.replace(/[\(\)\.\-\ ]/g, '');    

   if (fld.value == "") {
        error = "You didn't enter a phone number.\n";
        fld.style.background = 'Yellow';
    } else if (isNaN(parseInt(stripped))) {
        error = "The phone number contains illegal characters.\n";
        fld.style.background = 'Yellow';
    } else if (!(stripped.length == 10)) {
        error = "The phone number is the wrong length. Make sure you included an area code.\n";
        fld.style.background = 'Yellow';
    }
    return error;
}
</script>
</head>

<body>

	<div id="wrapper">
		
		<div id="sidebar">
			<?php $current = 5; include('sidebar.php'); ?>

		</div>

		<div id="main">
        
			<div class="headerImg"><img src="images/contactHeader.jpg" alt="Contact Us Header" /></div>

			<div id="GoogleMap"><a href="http://maps.google.com/maps/place?cid=10303646179647780534&q=dealer+online+marketing&hl=en" target="_blank"><img src="images/contactGoogleMap.jpg" alt="DOM Google Map" /></a></div>
  
	        <h1>Contact Us</h1>
            
            <p>If you think you are on your game or know that you are not, then let us perform a detailed Stress Test of your entire online presence. Please contact us today: 1-800-989-0298.<br /><br />
			Dealer Online Marketing helps car dealerships increase their Internet presence while driving more traffic to their website and to their store.</p> <br />
      
			<h5> Phone&#58; 864.248.0886&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;Fax&#58; 864.349.2119<br />
			<b>874 South Pleasantburg Drive, Suite 101 <br />
            Greenville, SC 29607</b>
            </h5>

			<div id="ContactForm">        
            <?php include('form2.php'); ?>            
            </div>	

	</div>

</body>
</html>
