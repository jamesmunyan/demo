<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="description" content="Dealer Online Marketing (DOM) drives automotive dealer advertising via the Internet and supports those efforts with traditional. Please call or contact us today 1-800-989-0298" />
	<meta name="google-site-verification" content="XfhfXepHLQ869Ug0MCIcvjI43UIGCbW9SXgUxkzNd6g" />

	<link href="css/screen.css" rel="stylesheet" type="text/css" media="screen" />
	<script type="text/javascript">document.write(unescape("%3Cscript src='" + (("https:" == document.location.protocol) ? "https" : "http") + "://c.mouseflow.com/projects/73353ce5-9eea-4e6a-8c10-0e57ded5cdad.js' type='text/javascript'%3E%3C/script%3E"));</script>
    <title>Dealer Online Marketing :: DDC</title>
</head>

<body>

	<div id="wrapper">
		
		<div id="sidebar">
			<?php $current = 0; include('sidebar.php'); ?>
		</div>

		<div id="main">
			<div class="headerImg"><img src="images/ddcHeader.jpg" alt="Digital Dealer Conference Header" /></div>
        
	        <p style="padding-top: 20px;">Dealer Online Marketing is excited to attend the 10th Digital Dealer Conference &amp; Exposition and looks forward to explaining the latest trends in digital marketing for the automotive field. Stop by booth 901 and let us show you how we can strengthen your online presence and generate more leads. In fact, if you contact us using the form below by April 6, we will prepare a custom presentation and website stress test that evaluates your online presence and pinpoints your dealership’s strengths and weaknesses. We’ll see you there!</p><br />

            <div id="ContactForm">
            <?php include('formDDC.php'); ?>
            </div>
        
		</div>
	</div>

</body>
</html>
