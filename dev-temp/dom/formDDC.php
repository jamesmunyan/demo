<html>
<head>
<link href="css/screen.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript">
function validateFormOnSubmit(theForm) {
var reason = "";

  reason += validateEmpty(theForm.first_name);
  reason += validateEmpty(theForm.last_name);
  reason += validateEmpty(theForm.company);
  reason += validateEmpty(theForm.email);
  reason += validateEmail(theForm.email);
  reason += validatePhone(theForm.phone);
  reason += validatePhone(theForm.mobile);
  
      
  if (reason != "") {
    alert("Some fields need correction:\n" + reason);
    return false;
  }

  return true;
}
function validateEmpty(fld) {
    var error = "";
 
    if (fld.value.length == 0) {
        fld.style.background = 'Yellow'; 
        error = "The required field has not been filled in.\n"
    } else {
        fld.style.background = 'White';
    }
    return error;  
}
function validateUsername(fld) {
    var error = "";
    var illegalChars = /\W/; // allow letters, numbers, and underscores
 
    if (fld.value == "") {
        fld.style.background = 'Yellow'; 
        error = "You didn't enter a username.\n";
    } else if ((fld.value.length < 5) || (fld.value.length > 15)) {
        fld.style.background = 'Yellow'; 
        error = "The username is the wrong length.\n";
    } else if (illegalChars.test(fld.value)) {
        fld.style.background = 'Yellow'; 
        error = "The username contains illegal characters.\n";
    } else {
        fld.style.background = 'White';
    }
    return error;
}
function validatePassword(fld) {
    var error = "";
    var illegalChars = /[\W_]/; // allow only letters and numbers 
 
    if (fld.value == "") {
        fld.style.background = 'Yellow';
        error = "You didn't enter a password.\n";
    } else if ((fld.value.length < 7) || (fld.value.length > 15)) {
        error = "The password is the wrong length. \n";
        fld.style.background = 'Yellow';
    } else if (illegalChars.test(fld.value)) {
        error = "The password contains illegal characters.\n";
        fld.style.background = 'Yellow';
    } else if (!((fld.value.search(/(a-z)+/)) && (fld.value.search(/(0-9)+/)))) {
        error = "The password must contain at least one numeral.\n";
        fld.style.background = 'Yellow';
    } else {
        fld.style.background = 'White';
    }
   return error;
}  
function trim(s)
{
  return s.replace(/^\s+|\s+$/, '');
}
function validateEmail(fld) {
    var error="";
    var tfld = trim(fld.value);                        // value of field with whitespace trimmed off
    var emailFilter = /^[^@]+@[^@.]+\.[^@]*\w\w$/ ;
    var illegalChars= /[\(\)\<\>\,\;\:\\\"\[\]]/ ;
   
    if (fld.value == "") {
        fld.style.background = 'Yellow';
        error = "You didn't enter an email address.\n";
    } else if (!emailFilter.test(tfld)) {              //test email for illegal characters
        fld.style.background = 'Yellow';
        error = "Please enter a valid email address.\n";
    } else if (fld.value.match(illegalChars)) {
        fld.style.background = 'Yellow';
        error = "The email address contains illegal characters.\n";
    } else {
        fld.style.background = 'White';
    }
    return error;
}
function validatePhone(fld) {
    var error = "";
    var stripped = fld.value.replace(/[\(\)\.\-\ ]/g, '');    

   if (fld.value == "") {
        error = "You didn't enter a phone number.\n";
        fld.style.background = 'Yellow';
    } else if (isNaN(parseInt(stripped))) {
        error = "The phone number contains illegal characters.\n";
        fld.style.background = 'Yellow';
    } else if (!(stripped.length == 10)) {
        error = "The phone number is the wrong length. Make sure you included an area code.\n";
        fld.style.background = 'Yellow';
    }
    return error;
}
</script></head> 
<body>
 <div id="content">
		<div style="text-align: right;">
              <form onSubmit="return validateFormOnSubmit(this)" action="sendmail2.php" method="post">
              <table width="250" border="0" cellpadding="10" cellspacing="0" >
              <tr>
                 
                
                <td align="left"><label for="first_name">First Name:</label><br/><input name="first_name" type="text"  id="first_name" size="40" maxlength="40" /></td>
                 <td rowspan="7" align="right" valign="top"><img src="http://www.dealeronlinemarketing.com/images/StressTestImage.png"  /><br></td>

              </tr>
              <tr>
          
                  
                <td align="left"><br/><label for="last_name">Last Name:</label><br/><input  id="last_name" maxlength="40" name="last_name" size="40" type="text" /></td>
              </tr>
              <tr>
           
                  
                <td align="left"><br/><label for="email">Email: </label><br/><input  id="email" maxlength="40" name="email" size="40" type="text" /></td>

              </tr>
              <tr>
             
                 
                <td align="left"><br/><label for="company">Company:</label><br/><input  id="company" maxlength="40" name="company" size="40" type="text" /></td>
              </tr>
              <tr>
                
                
                <td align="left"><br/><label for="phone">Phone: </label><br/><input  id="phone" maxlength="15" name="phone" size="40" type="text" /></td>

              </tr>
              <tr>
                  
                   
                <td align="left"><br/><label for="mobile">Mobile: </label><br/><input  id="mobile" maxlength="40" name="mobile" size="40" type="text" /></td>
              </tr>
              <tr>
                <td align="left"><br/><input type="submit" name="submit" value="Submit" /></td>
              </tr>

              </table>
              </form>
		</div>
    </div>
    </body></html>