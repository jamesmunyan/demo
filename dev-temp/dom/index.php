<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="google-site-verification" content="XfhfXepHLQ869Ug0MCIcvjI43UIGCbW9SXgUxkzNd6g" />

	<link href="css/screen.css" rel="stylesheet" type="text/css" media="screen" />
	<script type="text/javascript">document.write(unescape("%3Cscript src='" + (("https:" == document.location.protocol) ? "https" : "http") + "://c.mouseflow.com/projects/73353ce5-9eea-4e6a-8c10-0e57ded5cdad.js' type='text/javascript'%3E%3C/script%3E"));</script>
    <title>Automotive SEO | Internet Marketing Solutions for Auto Dealers</title>
    <meta name="keywords" content="Internet Marketing Solutions for Auto Dealers, Auto Marketing Firm, Automotive SEO" />
    <meta name="description" content="Dealer Online Marketing offers Internet marketing solutions for auto dealers like automotive SEO and much more. Get a free online presence analysis today." />
</head>

<body>

	<div id="wrapper">
		
		<div id="sidebar">
			<?php $current = 1; include('sidebar.php'); ?>
	  	</div>

		<div id="main">
        
			<div class="headerImgHome"><img src="images/homeHeader.jpg" alt="Homepage Header - Dealer Online Marketing Wall Sign" />
			</div>
        
	        <h1>Finally &mdash; one company that does it all!</h1>
        
	        <p>Dealer Online Marketing (DOM) is a digital automotive marketing agency that focuses on Internet Marketing. You can say we take the technology out of technology for the dealer. We focus on your &#8220;everything online&#8221; while you focus on selling cars. Our goal is crystal clear: to help car dealerships increase their lead volume, phone calls, walk-in traffic, and ultimately unit sales. Please contact us today: 1-800-989-0298.</p><br />
            
            <p>Dealer Online Marketing:<br />
            <ul>
            	<li><span style="color: #ed1c24; font-size: 90%; font-weight: bold; text-transform: uppercase;">Maximizes Your Technology</span><br />
                	We maximize the use of current technology, some of which you may already have in place.</li>

				<li><span style="color: #ed1c24; font-size: 90%; font-weight: bold; text-transform: uppercase;">Manages Your Online Inventory</span><br />
                	We make sure your inventory is found on all your sites, third-party sites, and the most popular Web sites out there. Complete with custom-written descriptions, high quality photos, and accurate prices.</li>

				<li><span style="color: #ed1c24; font-size: 90%; font-weight: bold; text-transform: uppercase;">Powers Up Your Search Engine Optimization &amp; Marketing</span><br />
                	This includes both on- and off-site SEO as well as Search Engine Marketing (SEM) both on and off search engines.</li>

				<li><span style="color: #ed1c24; font-size: 90%; font-weight: bold; text-transform: uppercase;">Creates &amp; Manages Social Media Sites</span><br />
                	We create and help maintain social sites for your dealership on sites like Facebook, Twitter, and YouTube, allowing you to build a community that will attract new customers to your site and inventory.</li>

    			<li><span style="color: #ed1c24; font-size: 90%; font-weight: bold; text-transform: uppercase;">Implements a Sensible Traditional Marketing Program</span><br />
                	The &#8220;traditional support&#8221;, as we call it, is a continuous and Internet-centric approach to traditional advertising.</li>

    			<li><span style="color: #ed1c24; font-size: 90%; font-weight: bold; text-transform: uppercase;">Help Your Internet Marketing Succeed</span><br />
                	We make sure your dealership’s presence is everywhere you want it to be found, all while complementing your strengths and compensating for (and overcoming) your business’ weaknesses.</li>
            </ul>

    			Please contact us today: 1-800-989-0298

            </p>

<!-- 					<ul class="featureBoxes">

				<li>
					<a href="about/team.html"><img src="images/HomeThumb_Team.jpg" alt="HomeThumb_Team" /></a>
				</li>
				<li>
					<a href="about/team.html"><img src="images/HomeThumb_Team.jpg" alt="HomeThumb_Team" /></a>
				</li>
				<li>
					<a href="about/team.html"><img src="images/HomeThumb_Team.jpg" alt="HomeThumb_Team" /></a>
				</li>
			</ul>
--> 

		</div>

	</div>

</body>
</html>
