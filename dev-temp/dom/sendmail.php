<?php 
session_start(); 

/* aqui se incializan variables de PHP */
if (phpversion() >= "4.2.0") {
        if ( ini_get('register_globals') != 1 ) {
                $supers = array('_REQUEST',
                                '_ENV',
                                '_SERVER',
                                '_POST',
                                '_GET',
                                '_COOKIE',
                                '_SESSION',
                                '_FILES',
                                '_GLOBALS' );
                                                                                
                foreach( $supers as $__s) {
                        if ( (isset($$__s) == true) && (is_array( $$__s
) == true) ) extract( $$__s, EXTR_OVERWRITE );
                }
                unset($supers);
        }
} else {
        if ( ini_get('register_globals') != 1 ) {
                                                                                
                $supers = array('HTTP_POST_VARS',
                                'HTTP_GET_VARS',
                                'HTTP_COOKIE_VARS',
                                'GLOBALS',
                                'HTTP_SESSION_VARS',
                                'HTTP_SERVER_VARS',
                                'HTTP_ENV_VARS'
                                 );
                                                                                
                foreach( $supers as $__s) {
                        if ( (isset($$__s) == true) && (is_array( $$__s
) == true) ) extract( $$__s, EXTR_OVERWRITE );
                }
                unset($supers);
        }
}

/*  DE AQUI EN ADELANTE PUEDES EDITAR EL ARCHIVO */

if($first_name=="")
{
	/* reclama si no se ha rellenado el campo email en el formulario */
	echo "put your name";
	exit();
}
if($last_name=="")
{
	/* reclama si no se ha rellenado el campo email en el formulario */
	echo "Put your last name";
	exit();
}
if($email=="")
{
	/* reclama si no se ha rellenado el campo email en el formulario */
	echo "Put your email";
	exit();
}

/* aquí se especifica la pagina de respuesta en caso de envío exitoso */
$respuesta="thanks.php";
// la respuesta puede ser otro archivo, en incluso estar en otro servidor

/* AQUÍ ESPECIFICAS EL CORREO AL CUAL QUEIRES QUE SE ENVÍEN LOS DATOS
DEL FORMULARIO, SI QUIERES ENVIAR LOS DATOS A MÁS DE UN CORREO,
LOS PUEDES SEPARAR POR COMAS */
$para ="james@dealeronlinemarketing.com";

/* AQUI ESPECIFICAS EL SUJETO (Asunto) DEL EMAIL */
$sujeto = "Contact Us Form Information from Dealer Online Marketing";

/* aquí se construye el encabezado del correo, en futuras
versiones del script explicaré mejor esta parte */ 
$encabezado = "From: $first_name $last_name <$email>";
$encabezado .= "\nReply-To: $email";
$encabezado .= "\nX-Mailer: PHP/" . phpversion();


/* con esto se captura la IP del que envío el mensaje */
$ip=$REMOTE_ADDR;

/* las siguientes líneas arman el mensaje */
$mensaje .= "Name: $first_name\n";
$mensaje .= "Last Name: $last_name\n";
$mensaje .= "e-mail: $email\n";
$mensaje .= "Company: $company\n";
$mensaje .= "City: $city\n";
$mensaje .= "State: $state\n";
$mensaje .= "Phone: $phone\n";
$mensaje .= "Mobile: $mobile\n";

/* Auto Responder */
$team = "The Dealer Online Marketing Team";
$para = "robert@dealeronlinemarketing.com";
$website = "www.dealeronlinemarketing.com";
$mynumber = "1-864-248-0886";

$subject = "Thank You For Contacting Dealer Online Marketing!";
$body = "<div align=\"center\"><div style=\"width: 600px; bgcolor: #ffffff; color: #000000; font-family: Arial, sans-serif; font-size: 12px; text-align: left;\"><a href=\"http://www.dealeronlinemarketing.com/\" target=\"new\"><img src=\"http://www.dealeronlinemarketing.com/header2.jpg\" border=\"0\"></a><div style=\"padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;\"><font style=\"font-family: Arial Black, Arial, sans-serif; font-size: 14px; color: #000000;\"><b>Hello $first_name $last_name,</b></font><br><br>Thank you for showing interest in our company. In doing so, you've taken your first step towards your business's future success.<br><br>When we’re working for you, you won’t have to worry about your dealership's online presence anymore—we'll take care of everything for you. We'll put ideas into motion, make changes to your website, send out monthly e-blasts with your sales specials, launch eBay auctions and Craigslist postings, create listings for your company on social networking sites, manage your online directories, and anything else you can dream up. What all this means for you is that you’ll save time and money, all while increasing your results two-fold and cutting your advertising budget in half. In short, our business is to make YOUR business better.<br><br>Again, thank you for getting in touch with us. We will be sure to contact you as soon as possible. If you can't wait, call us now at 1-800-989-0298<br><br>Have a great day,<br><br>$team<br>$mynumber<br>$website<br><br></div></div></div>";

$headers = "Content-Type: text/html; charset=iso-8859-1\nFrom: $para <$para>\nReply-To: <$para>\nReturn-Path: <$mymail>\nX-Mailer: PHP";

$myredirect = "http://www.dealeronlinemarketing.com/thanks.php";

/* Send Form, Send Auto Responder, and Redirect */
if (!mail($para, $sujeto, $mensaje, $encabezado)) echo "<h1>The message could not be sent</h1>";
else if ($email != "") {mail($email,$subject,$body,$headers); header("Location:$myredirect"); exit();}

?>