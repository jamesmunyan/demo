<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="google-site-verification" content="XfhfXepHLQ869Ug0MCIcvjI43UIGCbW9SXgUxkzNd6g" />
	<link href="css/screen.css" rel="stylesheet" type="text/css" media="screen" />
	<script type="text/javascript">document.write(unescape("%3Cscript src='" + (("https:" == document.location.protocol) ? "https" : "http") + "://c.mouseflow.com/projects/73353ce5-9eea-4e6a-8c10-0e57ded5cdad.js' type='text/javascript'%3E%3C/script%3E"));</script>
    <title>Internet Marketing Solutions for Auto Dealers | Automotive SEO</title>
    <meta name="keywords" content="Internet Marketing Solutions for Auto Dealers, Auto Marketing Firm, Automotive SEO" />
    <meta name="description" content="Dealer Online Marketing offers Internet marketing solutions for auto dealers like automotive SEO, SEM, social media, managing your online presence and more." />
	
</head>

<body>

	<div id="wrapper">
		
		<div id="sidebar">
			<?php $current = 4; include('sidebar.php'); ?>
		</div>

		<div id="main">
        
			<div class="headerImg"><img src="images/servicesHeader.jpg" alt="Services Header" /></div>
        
	        <h1>What We Do</h1>
            
			<ul id="ServiceColLeft">
				<h4>Website Inventory</h4>
					<li>Description Writing</li>
					<li>Generate New Finance/Lease, Used, Service, &amp; Parts Specials</li>
					<li>Create/Manage Flash Slideshow</li>
					<li>Build Out New Pages of Content</li>
					<li>Build Out &amp; Manage eBay Store</li>

				<h4>CRM</h4>
					<li>Create CRM Template Shell</li>
					<li>Create/Manage Automated Follow-up Program</li>
					<li>Create/Manage Template Library</li>

				<h4>Reputation/Social</h4>
					<li>Publish Testimonials To Blog</li>
					<li>Train Dealers On Internal Testimonial Procedures</li>
					<li>Create/Manage Social Presence: Facebook, Twitter, &amp; YouTube</li>

				<h4>Third Party</h4>
					<li>Launch Auctions to eBay</li>
					<li>Apply AutoTrader Spotlights</li>
					<li>Service, &amp; Parts Specials</li>
					<li>Refresh Cars.com &amp; AutoTrader.com</li>

    			<h4>Email Marketing</h4>
					<li>Dealership Newsletter</li>
					<li>eBlast Copywriting, Artwork, &amp; HTML Coding</li>
			</ul>

			<ul id="ServiceColRight">
			<h4>SEO/SEM</h4>
					<li>Managed Search Engine Optimization (SEO)</li>
					<li>Build Out &amp; Manage eBay Store</li>
					<li>Dealership Newsletter</li>
					<li>Create &amp; Host Microsites</li>
					<li>Managed Search Engine Marketing (SEM): <br />
                    	1 Google Certified employee specializing in Search Advertising, Reporting &amp; Analysis, &amp; Display Advertising</li>

				<h4>Value-Added Services</h4>
					<li>Vendor Management</li>
					<li>Technical Support</li>
					<li>Help Desk</li>
					<li>Select, Negotiate, &amp; Implement Vendors</li>
					<li>Monthly ROI Report</li>
					<li>Weekly Inventory Spot Checks</li>
					<li>Create/Host Microsites</li>
					<li>Secret Shop Lead Response Turnaround</li>

				<h4>Traditional Advertising Support</h4>
					<li>Traditional Advertising Strategy: Arbitron, Nielsen, &amp; Scarborough</li>
					<li>Qualitative &amp; Quantitative Marketing Sampling</li>
					<li>Print</li>
					<li>Logo &amp; Custom Artwork Creation</li>
					<li>Traditional Media Buy: Print, Radio, TV, &amp; Cable</li>
					<li>Radio Spot</li>
					<li>Post Edited TV Spot</li>
            </ul>

	</div>

</body>
</html>
