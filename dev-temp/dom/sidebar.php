<?php $active[$current] = “active”; ?>

<ul id="nav">
	<li><a href="index.php"><img src="images/domLogo.png" alt="Dealer Online Marketing Logo" /></a></li>
	<li <?php echo $active[1] ?> id="homeLink"><?php if ($current != 1) { echo "<a href=\"index.php\">Home</a>"; } else { echo "<span class=\"active\">Home</span>"; } ?></li>
	<li <?php echo $active[2] ?> id="aboutLink"><?php if ($current != 2) { echo "<a href=\"about.php\">About Us</a>"; } else { echo "<span class=\"active\">About Us</span>"; } ?></li>
	<li <?php echo $active[3] ?> id="workLink"><?php if ($current != 3) { echo "<a href=\"work.php\">Work</a>"; } else { echo "<span class=\"active\">Work</span>"; } ?></li>
	<li <?php echo $active[4] ?> id="servicesLink"><?php if ($current != 4) { echo "<a href=\"services.php\">Services</a>"; } else { echo "<span class=\"active\">Services</span>"; } ?></li>
	<li <?php echo $active[5] ?> id="contactLink"><?php if ($current != 5) { echo "<a href=\"contact.php\">Contact Us</a>"; } else { echo "<span class=\"active\">Contact Us</span>"; } ?></li>
	<li><hr /></li>
	<li id="phoneSales">Sales&#58; 800.989.0298</li>
	<li id="phoneService">Service&#58; 864.248.0886</li>
	<li style="margin: -5px 0 0 0; font-size: 38%; line-height: 13px; color:#666; font-weight:lighter">&copy; Dealer Online Marketing, 2010<br />All rights reserved.</li>
	<li><img src="images/internetDoneRight.png" alt="Internet Done Right Logo" /></li>
<!--<li>
		<ul id="social">
			<li><img src="images/facebook_25.png" alt="Facebook Link" /></li>
			<li><img src="images/twitter_25.png" alt="Twitter Link" /></li>
			<li><img src="images/linkedin_25.png" alt="LinkedIn Link" /></li>
			<li><img src="images/youtube_25.png" alt="YouTube Link" /></li>
        </ul>
    </li>-->
	<li id="googleACPbtn"><a href="https://adwords.google.com/professionals/profile/org?id=01137566398388529452&amp;hl=en"><img src="images/Google_ACP_Badge_90px.gif" /></a></li>
</ul>



<!--				<ul id="nav">
					<li><a href="index.html"><img src="images/domLogo.png" alt="Dealer Online Marketing Logo" /></a></li>
					<li id="homeLink"><a href="index.html">Home</a></li>
					<li id="aboutLink"><a href="about.html">About Us</a></li>
					<li id="workLink"><a href="work.html">Work</a></li>
					<li id="servicesLink"><a href="services.html">Services</a></li>
					<li id="contactLink" style="color: #666">Contact Us</li>
					<li><hr /></li>
					<li id="phoneSales">Sales&#58; 800.989.0298</li>
					<li id="phoneService">Service&#58; 864.248.0886</li>
					<li style="margin: -5px 0 0 0; font-size: 38%; line-height: 13px; color:#666; font-weight:lighter">&copy; Dealer Online Marketing, 2010<br />All rights reserved.</li>
					<li><img src="images/internetDoneRight.png" alt="Internet Done Right Logo" /></li>
					<li>
						<ul id="social">
							<li><img src="images/facebook_25.png" alt="Facebook Link" /></li>
							<li><img src="images/twitter_25.png" alt="Twitter Link" /></li>
							<li><img src="images/linkedin_25.png" alt="LinkedIn Link" /></li>
							<li><img src="images/youtube_25.png" alt="YouTube Link" /></li>
                        </ul>
                    </li>
 					<li id="googleACPbtn"><a href="https://adwords.google.com/professionals/profile/org?id=01137566398388529452&amp;hl=en"><img src="images/acp.gif"  /></a></li>
               </ul>
               
-->