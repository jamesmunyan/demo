<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="description" content="Dealer Online Marketing (DOM) drives automotive dealer advertising via the Internet and supports those efforts with traditional. Please call or contact us today 1-800-989-0298" />
	<meta name="google-site-verification" content="XfhfXepHLQ869Ug0MCIcvjI43UIGCbW9SXgUxkzNd6g" />

	<link href="css/screen.css" rel="stylesheet" type="text/css" media="screen" />
  	<script type="text/javascript">document.write(unescape("%3Cscript src='" + (("https:" == document.location.protocol) ? "https" : "http") + "://c.mouseflow.com/projects/73353ce5-9eea-4e6a-8c10-0e57ded5cdad.js' type='text/javascript'%3E%3C/script%3E"));</script>
    <title>Dealer Online Marketing</title>
</head>

<body>

	<div id="wrapper">
		
		<div id="sidebar">
			<?php $current = 5; include('sidebar.php'); ?>

		</div>

		<div id="main">
        
			<div class="headerImg"><img src="images/contactHeader.jpg" alt="Contact Us Header" /></div>

			<div id="GoogleMap"><a href="http://maps.google.com/maps/place?cid=10303646179647780534&q=dealer+online+marketing&hl=en" target="_blank"><img src="images/contactGoogleMap.jpg" alt="DOM Google Map" /></a></div>
  
	        <h1>Contact Us</h1>
            
            <p>If you think you are on your game or know that you are not, then let us perform a detailed Stress Test of your entire online presence. Please contact us today: 1-800-989-0298.<br /><br />
			Dealer Online Marketing helps car dealerships increase their Internet presence while driving more traffic to their website and to their store.</p> <br />
      
			<h5> Phone&#58; 800.989.0298&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;Fax&#58; 864.248.0886<br />
			<b>874 South Pleasantburg Drive, Suite 101 <br />
            Greenville, SC 29607</b>
            </h5>

			<div id="ContactForm">            
            Your form has been submitted. Thank you for contacting us!
            </div>

	</div>

</body>
</html>
