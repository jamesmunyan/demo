<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
   <meta name="google-site-verification" content="XfhfXepHLQ869Ug0MCIcvjI43UIGCbW9SXgUxkzNd6g" />

	<link href="css/screen.css" rel="stylesheet" type="text/css" media="screen" />
    <link rel="stylesheet" href="css/lightbox.css" type="text/css" media="screen" />

    <script type="text/javascript" src="js/prototype.js"></script>
	<script type="text/javascript" src="js/scriptaculous.js?load=effects,builder"></script>
	<script type="text/javascript" src="js/lightbox.js"></script>
	<script type="text/javascript">document.write(unescape("%3Cscript src='" + (("https:" == document.location.protocol) ? "https" : "http") + "://c.mouseflow.com/projects/73353ce5-9eea-4e6a-8c10-0e57ded5cdad.js' type='text/javascript'%3E%3C/script%3E"));</script>
	
     <title>Internet Marketing Solutions for Auto Dealers | Examples of Work</title>
    <meta name="keywords" content="Internet Marketing Solutions for Auto Dealers, Auto Marketing Firm, Automotive SEO" />
    <meta name="description" content="As a leading auto marketing firm, Dealer Online Marketing offers Internet marketing solutions for auto dealers. Get a free online presence analysis today." />
    
    
</head>

<body>

	<div id="wrapper">
		
		<div id="sidebar">
			<?php $current = 3; include('sidebar.php'); ?>
		</div>

		<div id="main">
        
<!--	        <h1>What We've Done</h1>	-->
			<div id="Thumbnails">
				<ul id="workColLeft">
					<li><a href="images/work/workSample01.jpg" rel="lightbox[work]" title="Various Slides">
        	                   	<img src="images/work/workThmbSample01.jpg" /></a></li>
					<li><a href="images/work/workSample02.jpg" rel="lightbox[work]" title="Big O Dodge Sales eBlast">
        	                   	<img src="images/work/workThmbSample02.jpg" /></a></li>
					<li><a href="images/work/workSample03.jpg" rel="lightbox[work]" title="Bill Currie Ford 3rd Party Banners">
        	                   	<img src="images/work/workThmbSample03.jpg" /></a></li>
					<li><a href="images/work/workSample04.jpg" rel="lightbox[work]" title="Planet Fiat Website Layout">
        	                   	<img src="images/work/workThmbSample04.jpg" /></a></li>
				</ul>
				<ul id="workColCenter">
					<li><a href="images/work/workSample05.jpg" rel="lightbox[work]" title="Ford EcoBoost Content Page &amp; Slide">
        	                   	<img src="images/work/workThmbSample05.jpg" /></a></li>
					<li><a href="images/work/workSample06.jpg" rel="lightbox[work]" title="Jim Hudson Hyundai Contest eBlast">
        	                   	<img src="images/work/workThmbSample06.jpg" /></a></li>
					<li><a href="images/work/workSample07.jpg" rel="lightbox[work]" title="Beau Townsend Ford Facebook Group Ad">
        	                   	<img src="images/work/workThmbSample07.jpg" /></a></li>
					<li><a href="images/work/workSample08.jpg" rel="lightbox[work]" title="Keeler Mini Microsite Layout">
        	                   	<img src="images/work/workThmbSample08.jpg" /></a></li>
				</ul>
               	<ul id="workColRight">
					<li><a href="images/work/workSample09.jpg" rel="lightbox[work]" title="Jaguar XKR Content Page">
        	                   	<img src="images/work/workThmbSample09.jpg" /></a></li>
					<li><a href="images/work/workSample10.jpg" rel="lightbox[work]" title="Jeep Event eBlast">
        	                   	<img src="images/work/workThmbSample10.jpg" /></a></li>
					<li><a href="images/work/workSample11.jpg" rel="lightbox[work]" title="Heritage Volkswagen Warranty Logo">
        	                   	<img src="images/work/workThmbSample11.jpg" /></a></li>
					<li><a href="images/work/workSample12.jpg" rel="lightbox[work]" title="Frank Porth Columbus Newspaper Ad">
        	                   	<img src="images/work/workThmbSample12.jpg" /></a></li>
				</ul>
			</div>

<!-- 					<ul class="featureBoxes">

				<li>
					<a href="about/team.html"><img src="images/HomeThumb_Team.jpg" alt="HomeThumb_Team" /></a>
				</li>
				<li>
					<a href="about/team.html"><img src="images/HomeThumb_Team.jpg" alt="HomeThumb_Team" /></a>
				</li>
				<li>
					<a href="about/team.html"><img src="images/HomeThumb_Team.jpg" alt="HomeThumb_Team" /></a>
				</li>
			</ul>
--> 

		</div>

	</div>

</body>
</html>
