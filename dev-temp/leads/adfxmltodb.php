<?php
//sets user name, passwords and server address.
require_once('auth_login.php');
//main functions for email processing.
require_once('email_functions.php');
//main functions for xml parsing.
require_once('parser_functions.php');

// Global vars set for logging in.
set_settings();

//open the mail box.
$mbox = get_email_open($authhost,$user,$pass);

//check if mail box is opened.
if($mbox!=false){
	
	//double check on mbox	
	$mailbox = imap_check($mbox);

	// Fetch an overview for all messages in inbox and get message id
	$email = imap_fetch_overview($mbox,"1:{$mailbox->Nmsgs}",0);
	
	//declare parsable var for messages. (leads array)
	$xml_messages = array();
	//declare bad email array
	$bad_emails = array();
	//delcare a counter for loop
	$counter = 0;
	
	//loop for unread.
	foreach ($email as $overview) {
		
		//clear vars for loop
		
		//customer
		$customer			=	'';
		$customer_name		=	'';
		$customer_email		=	'';
		$customer_phone		=	'';
		$customer_address	=	'';
		$customer_comments	=	'';
		
		//vehicle
		$vehicle			=	'';
		$vehicle_status		=	'';
		$vehicle_interest	=	'';
		$vehicle_year		=	'';
		$vehicle_make		=	'';
		$vehicle_model		=	'';
		$vehicle_stockno	=	'';
		$vehicle_vin		=	'';
		$vehicle_trim		=	'';
		$vehicle_price		=	'';		
		
		//vendor
		$vendor				=	'';
		//message vars
		$new_uid			=	'';
		$new_mid			=	'';
		$message			=	'';
				
		//check only unread.
		if($overview->seen=='0'){
			
			$xml_array = array();
			//getting info
			$new_uid = $overview->uid;
			$new_mid = $overview->msgno;
			
			//getting message (not marking read)
			$message = imap_body($mbox,$new_mid,FT_PEEK);
			
			if(simplexml_load_string($message)){
				
				//convert message that is xml to array of objects
				array_push($xml_messages,object_to_array(simplexml_load_string($message)));
				
				//search arrays for each lead's contact info broken down smaller to manage.
				if(search_nested_arrays($xml_messages[$counter], "customer")){
					$customer = search_nested_arrays($xml_messages[$counter], "customer");
					 
					if (search_nested_arrays($customer, "name")!= NULL){
						$customer_name = search_nested_arrays($customer, "name");						
					}else{
						$badstring = 'uid: '.$overview->uid.' date: '.$overview->date.' from: '.$overview->from.' subject: '.$overview->subject;
						array_push($bad_emails,$badstring);
					}
										
					$customer_email = search_nested_arrays($customer, "email");
					$customer_phone = search_nested_arrays($customer, "phone");
					$customer_address = search_nested_arrays($customer, "address");
					$customer_comments = search_nested_arrays($customer, "comments");					
					
				}else{
					$badstring = 'uid: '.$overview->uid.' date: '.$overview->date.' from: '.$overview->from.' subject: '.$overview->subject;
					array_push($bad_emails,$badstring);
				}
				
				//search arrays for each lead's vehicle info broken down smaller to manage.
				if(search_nested_arrays($xml_messages[$counter], "vehicle")){
					 $vehicle = search_nested_arrays($xml_messages[$counter], "vehicle");
					
					if (search_nested_arrays($vehicle, "vehicle")!= NULL){
						$vehicle_status		= search_nested_arrays($vehicle, "status");
						$vehicle_interest	= search_nested_arrays($vehicle, "interest");
						
						$vehicle_year		= search_nested_arrays($vehicle, "year");
						$vehicle_make		= search_nested_arrays($vehicle, "make");
						$vehicle_model		= search_nested_arrays($vehicle, "model");
						$vehicle_stockno	= search_nested_arrays($vehicle, "stock");
						$vehicle_vin		= search_nested_arrays($vehicle, "vin");
						$vehicle_trim		= search_nested_arrays($vehicle, "trim");
					
					}else{
						$badstring = 'uid: '.$overview->uid.' date: '.$overview->date.' from: '.$overview->from.' subject: '.$overview->subject;
						array_push($bad_emails,$badstring);
					}					
					
				}else{
					$badstring = 'uid: '.$overview->uid.' date: '.$overview->date.' from: '.$overview->from.' subject: '.$overview->subject;
					array_push($bad_emails,$badstring);
				}
				
				
				//update counter for next loop only if is well formatted xml.
				$counter = 	$counter + 1;
				
			}else{
				$badstring = 'uid: '.$overview->uid.' date: '.$overview->date.' from: '.$overview->from.' subject: '.$overview->subject;
				array_push($bad_emails,$badstring);
			}
						
		}//end if unread
		
	}//end foreach	
}//end if no inbox
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ADF/XML Inbox reader-parser</title>
</head>

<body>
<h1>Testing Stages</h1>
<pre>
<? print_r($vehicle[0]); ?>
</pre>
					 
</body>
</html>