<?php
//function to open the mailbox
function get_email_open($authhost,$user,$pass){	
	if($mbox=imap_open($authhost,$user,$pass)){
		return $mbox;
	}else{		
		return false;
	}
}

//function to close the mailbox
function get_email_close($mbox){
	if(imap_close($mbox)){
		return true;
	}else{
		return false;
	}	
}

//function to get the total number of messages
function get_email_total($mbox){
	$totalrows = imap_num_msg($mbox);
	
	if($totalrows>=1){
		return $totalrows;
	}else{
		return false;
	}
}

//function to get all the email headers (not the messages.)
function get_email_header($mbox,$msgno){
	$headers = imap_headerinfo($mbox, $msgno);
	return $headers;
}
?>