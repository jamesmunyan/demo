<?php
function XMLToArrayFlat($xml, &$return, $path='', $root=false)
{
    $children = array();
    if ($xml instanceof SimpleXMLElement) {
        $children = $xml->children();
        if ($root){ // we're at root
            $path .= '/'.$xml->getName();
        }
    }
    if ( count($children) == 0 ){
        $return[$path] = (string)$xml;
        return;
    }
    $seen=array();
    foreach ($children as $child => $value) {
        $childname = ($child instanceof SimpleXMLElement)?$child->getName():$child;
        if ( !isset($seen[$childname])){
            $seen[$childname]=0;
        }
        $seen[$childname]++;
        XMLToArrayFlat($value, $return, $path.'/'.$child.'['.$seen[$childname].']');
    }
}
?> 

<?php
/*$xml = simplexml_load_string('http://james.dealeronlinemarketing.com/leads/lead.xml');
$xmlarray = array(); // this will hold the flattened data
XMLToArrayFlat($xml, $xmlarray, '', true);
 foreach ($xmlarray as $key => $val) {
	 print_r($key.' = '.$val);
 }*/
?>

<!--You can also pull multiple files in one array:-->

<?php
$files = array();
$files[0] = 'http://james.dealeronlinemarketing.com/leads/lead.xml';
var_dump($files);
foreach($files as $file){
    $xml = simplexml_load_file($file);
	$xmlarray = array();
    XMLToArrayFlat($xml, $xmlarray, $file.':', true);	
}
foreach ($xmlarray as $key => $val) {
	$fix_key = str_replace('http://james.dealeronlinemarketing.com/leads/lead.xml:/adf/','',$key);
	//$fix_key = $key;
	print_r($fix_key.' = '.$val.'<br/><br/>');
}
?>