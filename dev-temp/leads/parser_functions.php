<?php
function search_nested_arrays($array, $key){
    if(is_object($array))
        $array = (array)$array;
   
    // search for the key
    $result = array();
    foreach ($array as $k => $value) {
        if(is_array($value) || is_object($value)){
            $r = search_nested_arrays($value, $key);
            if(!is_null($r))
                array_push($result,$r);
        }
    }
   
    if(array_key_exists($key, $array))
        array_push($result,$array[$key]);
   
   
    if(count($result) > 0){
        // resolve nested arrays
        $result_plain = array();
        foreach ($result as $k => $value) {
            if(is_array($value))
                $result_plain = array_merge($result_plain,$value);
            else
                array_push($result_plain,$value);
        }
        return $result_plain;
    }
    return NULL;
}

function object_to_array($d) {
	if (is_object($d))
	$d = get_object_vars($d);
    return is_array($d) ? array_map(__METHOD__, $d) : $d;
}

function array_to_object($d) {
	return is_array($d) ? (object) array_map(__METHOD__, $d) : $d;
}


function XMLToArrayFlat($xml, &$return, $path='', $root=false)
{
    $children = array();
    if ($xml instanceof SimpleXMLElement) {
        $children = $xml->children();
        if ($root){ // we're at root
            $path .= '/'.$xml->getName();
        }
    }
    if ( count($children) == 0 ){
        $return[$path] = (string)$xml;
        return;
    }
    $seen=array();
    foreach ($children as $child => $value) {
        $childname = ($child instanceof SimpleXMLElement)?$child->getName():$child;
        if ( !isset($seen[$childname])){
            $seen[$childname]=0;
        }
        $seen[$childname]++;
        XMLToArrayFlat($value, $return, $path.'/'.$child.'['.$seen[$childname].']');
    }
}
?>