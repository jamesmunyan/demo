<?php require_once('Connections/DOMSigGen.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "index.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form3")) {
  $insertSQL = sprintf("INSERT INTO Templates (ClientId, TemplateCode) VALUES (%s, %s)",
                       GetSQLValueString($_POST['ClientId'], "int"),
                       GetSQLValueString($_POST['TemplateCode'], "text"));

  mysql_select_db($database_DOMSigGen, $DOMSigGen);
  $Result1 = mysql_query($insertSQL, $DOMSigGen) or die(mysql_error());

  $insertGoTo = "addtemplates.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE Templates SET TemplateCode=%s WHERE Id=%s",
                       GetSQLValueString($_POST['TemplateCode'], "text"),
                       GetSQLValueString($_POST['Id'], "int"));

  mysql_select_db($database_DOMSigGen, $DOMSigGen);
  $Result1 = mysql_query($updateSQL, $DOMSigGen) or die(mysql_error());

  $updateGoTo = "addtemplates.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_UserInfo = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_UserInfo = $_SESSION['MM_Username'];
}
mysql_select_db($database_DOMSigGen, $DOMSigGen);
$query_UserInfo = sprintf("SELECT * FROM Users WHERE UserName = %s", GetSQLValueString($colname_UserInfo, "text"));
$UserInfo = mysql_query($query_UserInfo, $DOMSigGen) or die(mysql_error());
$row_UserInfo = mysql_fetch_assoc($UserInfo);
$totalRows_UserInfo = mysql_num_rows($UserInfo);

$colname_Template = "-1";
if (isset($_GET['recordId'])) {
  $colname_Template = $_GET['recordId'];
}
mysql_select_db($database_DOMSigGen, $DOMSigGen);
$query_Template = sprintf("SELECT * FROM Templates WHERE ClientId = %s", GetSQLValueString($colname_Template, "int"));
$Template = mysql_query($query_Template, $DOMSigGen) or die(mysql_error());
$row_Template = mysql_fetch_assoc($Template);
$totalRows_Template = mysql_num_rows($Template);

$colname_Clients = "-1";
if (isset($_GET['recordId'])) {
  $colname_Clients = $_GET['recordId'];
}
mysql_select_db($database_DOMSigGen, $DOMSigGen);
$query_Clients = sprintf("SELECT * FROM Clients WHERE Id = %s", GetSQLValueString($colname_Clients, "int"));
$Clients = mysql_query($query_Clients, $DOMSigGen) or die(mysql_error());
$row_Clients = mysql_fetch_assoc($Clients);
$totalRows_Clients = mysql_num_rows($Clients);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Dealer Online Marketing - Email Signature Generator</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="James Munyan">

    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
    
    <link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">

    <script src="lib/jquery-1.7.2.min.js" type="text/javascript"></script>

    <!-- Demo page code -->

    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .brand { font-family: georgia, serif; }
        .brand .first {
            color: #ccc;
            font-style: italic;
        }
        .brand .second {
            color: #fff;
            font-weight: bold;
        }
    </style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
  <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
  <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
  <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!--> 
<body class=""> 
  <!--<![endif]-->
    
	<div class="navbar">
    	<div class="navbar-inner">
        	<ul class="nav pull-right">
				<!--<li><a href="#" class="hidden-phone visible-tablet visible-desktop" role="button">Settings</a></li>-->
                <li id="fat-menu" class="dropdown">
                	<a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">
                    	<i class="icon-user"></i> <i class="icon-caret-down"></i>
                    </a>
					<ul class="dropdown-menu">
                        <!-- <li><a tabindex="-1" href="#">My Account</a></li>
                        <li class="divider"></li>
                        <li><a tabindex="-1" class="visible-phone" href="#">Settings</a></li>
                        <li class="divider visible-phone"></li>-->
                        <li>Logout</li>
               	  </ul>
			  </li>
			</ul>
            <a class="brand" href="welcome.php"><span class="first">Dealer Online Marketing</span></a>
        </div>
    </div>
    <div class="sidebar-nav">
    	<a href="#dashboard-menu" class="nav-header" data-toggle="collapse"><i class="icon-dashboard"></i>Menu</a>
        <ul id="dashboard-menu" class="nav nav-list collapse in">
            <li><a href="welcome.php">Welcome</a></li>
            <li><a href="clientlist.php">Generator</a></li>
            <li><a href="addtemplates.php">Templates</a></li>
            <li ><a href="addclient.php">Add A Client</a></li>
        </ul>
	</div>
     <div class="content">
        <div class="header">
            <h1 class="page-title">Add / Change Signature Template</h1>
        </div>
        <ul class="breadcrumb">
            <li><a href="welcome.php">Home</a> <span class="divider">/</span></li>
            <li class="active"><a href="addtemplates.php">Client List</a> <span class="divider">/</span></li>
            <li class="active">Add / Edit Template</li>
        </ul>
        <div class="container-fluid">
            <div class="row-fluid">
			  <div class="block span">
                	<p class="block-heading">Add / Change A Signature Template</p>
                    <div class="block-body">
                    <h2>Add / Change Email Signature Template For <?php echo $row_Clients['Name']; ?></h2>
                    <?php 
					if($totalRows_Template == 0){  ?>
                    <form method="post" name="form3" action="<?php echo $editFormAction; ?>">
                    <label>Paste HTML Code for Email Template</label>
                    <textarea name="TemplateCode" style="width:600px; height:350px;" ></textarea>
                    <br>
					<a onClick="document.form3.submit();" class="btn btn-primary pull-right" tabindex="3">Save</a>
                    <input type="hidden" name="ClientId" value="<?php echo $row_Clients['Id']; ?>">
                    <input type="hidden" name="MM_insert" value="form3">
                    </form>
                    <br>
                    <?php }elseif($totalRows_Template==1){ ?>
                    
                    <form method="post" name="form1" action="<?php echo $editFormAction; ?>">
                    <label>Paste HTML Code for Email Template</label><br>
                    
                    <textarea name="TemplateCode" style="width:800px; height:350px;" ><?php echo htmlentities($row_Template['TemplateCode'], ENT_COMPAT, 'utf-8'); ?></textarea><br>

                    <span style="color:#F00;"><strong>NOTE:</strong> Merge tags must be in <strong>%tag%</strong> style and match column names in csv file.</span>
                    <input type="hidden" name="MM_update" value="form1">
                    <input type="hidden" name="Id" value="<?php echo $row_Template['Id']; ?>">
                    <br>
					<a onClick="document.form1.submit();" class="btn btn-primary pull-right" tabindex="3">Save</a>
                    </form>
                    <br>
<?php } ?>
                    </div>
			</div>
        
            <footer>
                <hr>
            </footer>
		</div>
	</div>
</div>
<script src="lib/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
    
</body>
</html>
<?php
mysql_free_result($UserInfo);

mysql_free_result($Template);

mysql_free_result($Clients);
?>
