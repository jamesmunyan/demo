<?php require_once('Connections/DOMSigGen.php'); ?>
<?php
//initialize the session
if (!isset($_SESSION)) {
  session_start();
}

// ** Logout the current user. **
$logoutAction = $_SERVER['PHP_SELF']."?doLogout=true";
if ((isset($_SERVER['QUERY_STRING'])) && ($_SERVER['QUERY_STRING'] != "")){
  $logoutAction .="&". htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_GET['doLogout'])) &&($_GET['doLogout']=="true")){
  //to fully log out a visitor we need to clear the session varialbles
  $_SESSION['MM_Username'] = NULL;
  $_SESSION['MM_UserGroup'] = NULL;
  $_SESSION['PrevUrl'] = NULL;
  unset($_SESSION['MM_Username']);
  unset($_SESSION['MM_UserGroup']);
  unset($_SESSION['PrevUrl']);
	
  $logoutGoTo = "index.php";
  if ($logoutGoTo) {
    header("Location: $logoutGoTo");
    exit;
  }
}
?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "index.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_UserInfo = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_UserInfo = $_SESSION['MM_Username'];
}
mysql_select_db($database_DOMSigGen, $DOMSigGen);
$query_UserInfo = sprintf("SELECT * FROM Users WHERE UserName = %s", GetSQLValueString($colname_UserInfo, "text"));
$UserInfo = mysql_query($query_UserInfo, $DOMSigGen) or die(mysql_error());
$row_UserInfo = mysql_fetch_assoc($UserInfo);
$totalRows_UserInfo = mysql_num_rows($UserInfo);

$colname_Clients = "-1";
if (isset($_POST['ClientId'])) {
  $colname_Clients = $_POST['ClientId'];
}
mysql_select_db($database_DOMSigGen, $DOMSigGen);
$query_Clients = sprintf("SELECT * FROM Clients WHERE Id = %s", GetSQLValueString($colname_Clients, "int"));
$Clients = mysql_query($query_Clients, $DOMSigGen) or die(mysql_error());
$row_Clients = mysql_fetch_assoc($Clients);
$totalRows_Clients = mysql_num_rows($Clients);

$colname_Templates = "-1";
if (isset($_POST['ClientId'])) {
  $colname_Templates = $_POST['ClientId'];
}
mysql_select_db($database_DOMSigGen, $DOMSigGen);
$query_Templates = sprintf("SELECT * FROM Templates WHERE ClientId = %s", GetSQLValueString($colname_Templates, "int"));
$Templates = mysql_query($query_Templates, $DOMSigGen) or die(mysql_error());
$row_Templates = mysql_fetch_assoc($Templates);
$totalRows_Templates = mysql_num_rows($Templates);
?>
<?php
function csv_to_array($filename='', $delimiter=',')
{
    if(!file_exists($filename) || !is_readable($filename))
        return FALSE;

    $header = NULL;
    $data = array();
    if (($handle = fopen($filename, 'r')) !== FALSE)
    {
        while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
        {
            if(!$header)
                $header = $row;
            else
                $data[] = array_combine($header, $row);
        }
        fclose($handle);
    }
    return $data;
}


?>
<?php
$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}
//Process and upload file for storage and usage as array from function above.
if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
	$csv = array();
	if(isset($_POST['ClientId'])&&$_POST['ClientId']>0){
	//Сheck that we have a file
	if((!empty($_FILES["csv"])) && ($_FILES['csv']['error'] == 0)) {
		$filename = basename($_FILES['csv']['name']);
		$ext = substr($filename, strrpos($filename, '.') + 1);
  		if($ext == "csv") {
			//Determine the path to which we want to save this file
			if (!file_exists(dirname(__FILE__).'/lists/'.$row_Clients['Code'])) {
				mkdir(dirname(__FILE__).'/lists/'.$row_Clients['Code'], 0777, true);
			}
			$dirlink = dirname(__FILE__).'/lists/'.$row_Clients['Code'];
			chmod($dirlink,0777);
			$newname = $dirlink.'/'.$filename;
			if (file_exists($newname)) {
    			unlink($newname);
				//echo "The file $filename exists";
			} else {
				//echo "The file $filename does not exist";
			}
			//Check if the file with the same name is already exists on the server
			if (!file_exists($newname)) {
				//Attempt to move the uploaded file to it's new place
				if ((move_uploaded_file($_FILES['csv']['tmp_name'],$newname))) {
					$csv_array = csv_to_array($newname,',');
					
					} else {
					       $error =  "Error: A problem occurred during file upload!";
						   $errorCount = '1';
					}
				} else {
					$error = "Error: File ".$_FILES["csv"]["name"]." already exists";
					$errorCount = '1';
					
				}
			} else {
				$error = "Error: Only .csv files are accepted for upload";
				$errorCount = '1';
			}
		} else {
			$error = "Error: No file uploaded";
		}
	}
}
  /*$insertSQL = sprintf("INSERT INTO Clients (Name, Code) VALUES (%s, %s)",
                       GetSQLValueString($_POST['Name'], "text"),
                       GetSQLValueString($_POST['Code'], "text"));

  mysql_select_db($database_DOMSigGen, $DOMSigGen);
  $Result1 = mysql_query($insertSQL, $DOMSigGen) or die(mysql_error());

  $insertGoTo = "addtemplates.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}*/
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Dealer Online Marketing - Email Signature Generator</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="James Munyan">

    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
    
    <link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">

    <script src="lib/jquery-1.7.2.min.js" type="text/javascript"></script>

    <!-- Demo page code -->

    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .brand { font-family: georgia, serif; }
        .brand .first {
            color: #ccc;
            font-style: italic;
        }
        .brand .second {
            color: #fff;
            font-weight: bold;
        }
    </style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
  <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
  <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
  <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!--> 
<body class=""> 
  <!--<![endif]-->
    
	<div class="navbar">
    	<div class="navbar-inner">
        	<ul class="nav pull-right">
				<!--<li><a href="#" class="hidden-phone visible-tablet visible-desktop" role="button">Settings</a></li>-->
                <li id="fat-menu" class="dropdown">
                	<a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">
                    	<i class="icon-user"></i><?php echo $row_UserInfo['UserName']; ?>
                    	<i class="icon-caret-down"></i>
                    </a>
					<ul class="dropdown-menu">
                        <!-- <li><a tabindex="-1" href="#">My Account</a></li>
                        <li class="divider"></li>
                        <li><a tabindex="-1" class="visible-phone" href="#">Settings</a></li>
                        <li class="divider visible-phone"></li>-->
                        <li><a href="<?php echo $logoutAction ?>">Logout</a></li>
                   	</ul>
				</li>
			</ul>
            <a class="brand" href="welcome.php"><span class="first">Dealer Online Marketing</span></a>
        </div>
    </div>
    <div class="sidebar-nav">
    	<a href="#dashboard-menu" class="nav-header" data-toggle="collapse"><i class="icon-dashboard"></i>Menu</a>
        <ul id="dashboard-menu" class="nav nav-list collapse in">
            <li><a href="welcome.php">Welcome</a></li>
            <li><a href="clientlist.php">Generator</a></li>
            <li><a href="addtemplates.php">Templates</a></li>
            <li ><a href="addclient.php">Add A Client</a></li>
        </ul>
	</div>
    <div class="content">
        <div class="header">
            <h1 class="page-title">Generated Signatures</h1>
        </div>
        <ul class="breadcrumb">
            <li><a href="welcome.php">Home</a> <span class="divider">/</span></li>
            <li class="active">Welcome</li>
        </ul>
        <div class="container-fluid">
            <div class="row-fluid">
				<div class="block span">
                	<p class="block-heading">Signatures</p>
                    <div class="block-body">
                    <h2>Signatures</h2>
                    <span>See below for signatures.</span><br>

                    <p>
					
                    <? 
					$template = $row_Templates['TemplateCode'];

					$re1='.*?';	# Non-greedy match on filler
					$re2='(%)';	# Any Single Character 1
					$re3='((?:[a-z][a-z0-9_]*))';	# Variable Name 1
					$re4='(%)';	# Any Single Character 2
					
					if ($c=preg_match_all ("/".$re1.$re2.$re3.$re4."/is", $template, $matches))
					{
						$c1=$matches[1][0];
						$var1=$matches[2][0];
						$c2=$matches[3][0];
						//print "($c1) ($word1) ($c2) \n";
					}
					$mergetags = $matches[2];
					$headertags = array_keys($csv_array[1]);
					if(array_intersect($mergetags,$headertags) === $mergetags) {
						$ok = 'TRUE';
					}else{
						$ok = 'FALSE';
					}
					$template_counter=0;
					$Sig_Code='';
					$Temp_Code=@$template;
					$Links = '';
					$mask = dirname(__FILE__).'/lists/'.$row_Clients['Code'].'/*.html';
   					array_map( "unlink", glob( $mask ) );
					$main_sig_file = dirname(__FILE__).'/lists/'.$row_Clients['Code'].'/Signatures.html';
					//$base_url = str_replace($DOCUMENT_ROOT, "", dirname($PHP_SELF));
					
					$main_sig_handle = fopen($main_sig_file, 'w') or die('Cannot open file:  '.$main_sig_file);
					chmod($main_sig_file,0777);
					
					if($ok==='TRUE'){
						// $csv_array break main array into seperate arrays
						foreach ($csv_array as $csv_subarray) {
							//apply commenting for signatures
							$Temp_Code='<!--Signature Block-->'.@$template.'<!--Signature Block--><br/>';
							//set counter to add to help.
							$template_counter = $template_counter+1;
							//create sub files
							$sub_sig_file = dirname(__FILE__).'/lists/'.$row_Clients['Code'].'/'.$template_counter.'_Single.html';
							$sub_sig_handle = fopen($sub_sig_file, 'w') or die('Cannot open file:  '.$sub_sig_file);
							chmod($main_sig_file,0777);
							//break sub arrays into values.
							foreach ($csv_subarray as $csv_key => $csv_value){
								//replace %tag% with values from csv file that match.
								$Temp_Code = str_replace('%'.$csv_key.'%',$csv_value,$Temp_Code);
							}
							//append each block to the main file with all signatures.
							$Sig_Code .= $Temp_Code;
							if(fwrite($sub_sig_handle, $Temp_Code)){
								$Links .= '<a href="http://www.dealeronlinemarketing.com/DomSigGen/lists/'.$row_Clients['Code'].'/'.$template_counter.'_Single.html" target="new">http://www.dealeronlinemarketing.com/DomSigGen/lists/'.$row_Clients['Code'].'/'.$template_counter.'_Single.html'.'</a><br/>'; 
							}
							//echo '<br/>';
							
							
						}
					}else{
						$merge_error=='There was a issue with merging the template to the provided csv file.';
					}
					if(fwrite($main_sig_handle, $Sig_Code)){
						echo 'File with all signature blocks.<br/>';
						echo '<a href="http://www.dealeronlinemarketing.com/DomSigGen/lists/'.$row_Clients['Code'].'/Signatures.html" target="new">Complete Signatures File</a><br/><br/> Real Path : http://www.dealeronlinemarketing.com/DomSigGen/lists/'.$row_Clients['Code'].'/Signatures.html<br/><br/>'; 
					}
					//echo $Sig_Code;
					echo 'List of Links for each signature separate.<br/>';
					echo $Links;
					?>
					</p>
                </div>
			</div>
        
            <footer>
                <hr>
            </footer>
		</div>
	</div>
</div>
    <script src="lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
    
  </body>
</html>
<?php
mysql_free_result($UserInfo);

mysql_free_result($Clients);

mysql_free_result($Templates);
?>
