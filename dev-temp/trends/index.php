<!DOCTYPE html>
<html>
<head>

  <title>Google Trends - Hot Searches</title>
  
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  
  <meta name="description" content="A visualization of the latest hot searches from Google Trends.">
  <meta name="viewport" content="width=device-width, user-scalable=no">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta name="google" value="notranslate" />
  
  <link rel="stylesheet" href="css/style.css">
  
  <script src="js/vendor/modernizr-2.6.2.min.js"></script>

</head>
<body>
<!-- 
http://www.google.com/calendar/feeds/developer-calendar@google.com/public/full?alt=json 
http://www.google.com/trends/hottrends/atom/feed?pn=p1
https://ajax.googleapis.com/ajax/services/feed/load?v=1.0&q=http%3A%2F%2Fwww.google.com%2Ftrends%2Fhottrends%2Fatom%2Fhourly&callback=processResults

-->
<!--[if lt IE 8]>
      <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
  <![endif]-->

  <div id="matrix-container">
  
    

    
    <div id="matrix-button" class="idleable"></div>
    <div id="desc" class="idleable">Showing the latest hot searches in <div id="region"><span>All Regions</span></div>.</div>

    <div id="matrix-select-container"></div>
    
    <select id="region-select">
      <option value="0">All Regions</option>
      <option value="8" class="sort">Australia</option>
      <option value="13" class="sort">Canada</option>
      <option value="15" class="sort">Germany</option>
      <option value="10" class="sort">Hong Kong</option>
      <option value="3" class="sort">India</option>
      <option value="6" class="sort">Israel</option>
      <option value="4" class="sort">Japan</option>
      <option value="14" class="sort">Russia</option>
      <option value="5" class="sort">Singapore</option>
      <option value="12" class="sort">Taiwan</option>
      <option value="9" class="sort">United Kingdom</option>
      <option value="1" class="sort">United States</option>
    </select>
    

  </div>

  

  <div id="logo">
    <a href="http://www.google.com/trends/hottrends">
      <span>Trends</span>
    </a>
  </div>

  

  <script src="js/vendor/jquery.min.js"></script>
  <script src="js/vendor/plugins.js"></script>
  <script src="js/vendor/underscore.js" type="text/javascript"></script>
  <script src="js/vendor/classList.js" type="text/javascript"></script>
  <script src="js/vendor/jquery.easing.1.3.js" type="text/javascript"></script>
  <script src="js/vendor/raf.js" type="text/javascript"></script>
  <script src="js/vendor/url.js" type="text/javascript"></script>
  
  <script src="js/setOffset.js" type="text/javascript"></script>
  <script src="js/transitionOffset.js" type="text/javascript"></script>
  <script src="js/Blinker.js" type="text/javascript"></script>
  <script src="js/rtl.js" type="text/javascript"></script>
  <script src="js/Wiper.js" type="text/javascript"></script>
  <script src="js/Typer.js" type="text/javascript"></script>
  
  <script src="js/main.js" type="text/javascript"></script>

  
</body>
</html>